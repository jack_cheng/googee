# googee

#### 介绍

googee使用[go-zero](https://github.com/zeromicro/go-zero)作为微服务底层框架,对于小的项目使用docker-compser来搭建生产环境。大一点的项目使用k8s搭建生产环境，项目的大部分代码和项目结构参照了，[go-zero-looklook](https://github.com/Mikaelemmmm/go-zero-looklook),使用docker-composer作为开发环境，代码支持热更新

#### 软件架构

#### 安装教程

命令行进入到googee目录下执行如下命令

安装项目依赖的环境

```bash
docker-compose -f docker-compose.yml up -d
```

安装开发环境

```bash
docker-compose up -d
```

#### 导入数据库sql

导入dtm依赖的数据文件dtm-sql目录下的dtm.sql导入到mysql中
导入所有model目录下的sql文件

#### 服务列表

- [x] 用户服务

- [x] 账户服务

- [x] 短信服务

- [x] 验证码服务

- [x] 地址服务

- [x] 排行榜服务

- [x] 收件箱-实现的一般，质量不高，需要合并到消息中心当中去

- [x] 任务服务

- [x] 检索服务

- [ ] 聊天服务和好友

- [ ] 实名认证服务
  
  - [ ] 支付宝

- [ ] 消息服务
  
  - [ ] 消息中心

- [ ] 微博服务
  
  - [ ] 支持短视频
  
  - [ ] 微博
  
  - [ ] 朋友圈

- [ ] 商城
  
  - [ ] SKU
  
  - [ ] 优惠券服务
  
  - [ ] 运费服务
  
  - [ ] 售后服务
  
  - [ ] 支付服务
  
  - [ ] 秒杀
  
  - [ ] 团购

- [ ] 直播

- [ ] 评论服务

- [ ] 对象存储服务
  
  - [ ] OSS

- [ ] 帮助中心

- [ ] 公会服务

 

#### 端口说明

| service name     | port |
| ---------------- | ---- |
| account-api      | 4001 |
| account-rpc      | 4002 |
| captcha-rpc      | 4003 |
| leaderboard-api  | 4005 |
| leaderboard-rpc  | 4006 |
| mqueue-scheduler | 3002 |
| mqueue-job       | 3003 |
| sms-api          | 4007 |
| sms-rpc          | 4008 |
| user-api         | 4009 |
| user-rpc         | 4010 |
| addrbook-api     | 4011 |
| addrbook-rpc     | 4012 |
| inbox-api        | 4013 |
| inbox-rpc        | 4014 |
| task-api         | 4015 |
| task-rpc         | 4016 |
| search-api       | 4017 |
| search-rpc       | 4018 |
| friend-api       | 4019 |
| friend-rpc       | 4020 |
| fans-api         | 4021 |
| fans-rpc         | 4022 |
| admin-api        | 3005 |
| sys-rpc          | 3006 |

#### TODO功能

- 账户的更改记录
- 账号注销以后，jwt失效
- 账号禁用以后，jwt失效



#### 注意事项

###### mysql8连接不上的解决办法

命令行进入到容器googee-mysql中执行

```bash
docker exec -it googee-mysql bash
```

连接mysql

```bash
mysql -uroot -pPXDN93VRKUm8TeE7
```

更新mysql的配置

```sql
use mysql;
grant all privileges on *.* to 'root'@'%' with grant option;
update user set host='%' where user='root';
grant all privileges on *.* to 'root'@'%';
flush privileges;
update user set host='%' where user='root';
grant all privileges on *.* to 'root'@'%';
```

####测试

测试项目需要先搭建好测试环境，然后在t目录下执行

```bash
go test ./...
```


#### 生成Swagger 

用户系统
```bash
goctl api plugin -plugin goctl-swagger="swagger -filename user.json" -api ./service/user/api/user.api -dir ./doc
```

账户系统

```bash
goctl api plugin -plugin goctl-swagger="swagger -filename account.json" -api ./service/account/api/account.api -dir ./doc
```

#### 启动swagger-ui

```bash
 docker run --rm -p 8083:8080 -e SWAGGER_JSON=/foo/user.json -v $PWD/doc:/foo swaggerapi/swagger-ui
```