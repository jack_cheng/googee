package logic

import (
	"context"

	"googee/common/errorx"
	"googee/service/account/api/internal/svc"
	"googee/service/account/api/internal/types"
	"googee/service/account/rpc/account"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type InfoLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewInfoLogic(ctx context.Context, svcCtx *svc.ServiceContext) *InfoLogic {
	return &InfoLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *InfoLogic) Info(req *types.UserInfoReq) (resp *types.UserInfoReply, err error) {
	resp = &types.UserInfoReply{}

	//TODO：检查被查看的人是否存在，是否为一个有效的用户

	result, err := l.svcCtx.Account.GetAccount(l.ctx, &account.IdRequest{
		Id: req.Id,
	})

	if result == nil {
		return nil, errorx.NewCodeErrorNoMsg(errorx.ACCOUNT_ERROR + 1)
	}

	if err != nil {
		st, ok := status.FromError(err)
		if ok {
			if st.Code() == codes.NotFound {
				return nil, errorx.NewCodeErrorNoMsg(errorx.ACCOUNT_ERROR + 1)
			} else if st.Code() == codes.Internal {
				return nil, errorx.NewCodeError(errorx.INTERNAL_ERROR, "系统内部未知错误")
			}
		} else {
			return nil, errorx.NewCodeError(errorx.INTERNAL_ERROR, "系统内部未知错误")
		}
	}

	copier.Copy(&resp.Data, result)
	return
}
