package svc

import (
	"googee/service/account/api/internal/config"
	"googee/service/account/rpc/account"
	"googee/service/user/rpc/user"

	"github.com/zeromicro/go-zero/zrpc"
)

type ServiceContext struct {
	Config  config.Config
	Account account.Account
	User    user.User
}

func NewServiceContext(c config.Config) *ServiceContext {
	return &ServiceContext{
		Config:  c,
		Account: account.NewAccount(zrpc.MustNewClient(c.Account)),
		User:    user.NewUser(zrpc.MustNewClient(c.User)),
	}
}
