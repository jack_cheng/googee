package svc

import (
	"googee/service/account/model"
	"googee/service/account/rpc/internal/config"

	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

type ServiceContext struct {
	Config          config.Config
	AccountModel    model.AccountModel
	AccountLogModel model.AccountLogModel
	SqlConn         sqlx.SqlConn
}

func NewServiceContext(c config.Config) *ServiceContext {
	conn := sqlx.NewMysql(c.Mysql.DataSource)
	return &ServiceContext{
		Config:          c,
		AccountModel:    model.NewAccountModel(conn, c.CacheRedis),
		AccountLogModel: model.NewAccountLogModel(conn, c.CacheRedis),
		SqlConn:         conn,
	}
}
