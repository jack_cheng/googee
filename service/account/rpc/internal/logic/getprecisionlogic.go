package logic

import (
	"context"
	"reflect"

	"googee/service/account/rpc/internal/svc"
	"googee/service/account/rpc/types/account"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetPrecisionLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetPrecisionLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetPrecisionLogic {
	return &GetPrecisionLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// 获得精度
func (l *GetPrecisionLogic) GetPrecision(in *account.Empty) (resp *account.PrecisionResponse, err error) {
	resp = &account.PrecisionResponse{}
	t := reflect.TypeOf(l.svcCtx.Config.Digital)
	v := reflect.ValueOf(l.svcCtx.Config.Digital)

	resp.Precision = make(map[string]uint64)
	for k := 0; k < t.NumField(); k++ {
		sk := t.Field(k).Name
		sv := v.Field(k).Uint()

		resp.Precision[sk] = sv
	}
	return resp, nil
}
