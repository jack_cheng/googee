package logic

import (
	"context"

	"googee/service/account/rpc/internal/svc"
	"googee/service/account/rpc/types/account"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/core/stores/sqlc"
)

type GetAccountLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetAccountLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetAccountLogic {
	return &GetAccountLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// 获得用户的账户信息
func (l *GetAccountLogic) GetAccount(in *account.IdRequest) (resp *account.IdResponse, err error) {
	resp = &account.IdResponse{}
	accountmodel, err := l.svcCtx.AccountModel.FindOne(l.ctx, in.Id)

	if err == sqlc.ErrNotFound {
		return nil, nil
	}

	copier.Copy(resp, accountmodel)
	return resp, nil
}
