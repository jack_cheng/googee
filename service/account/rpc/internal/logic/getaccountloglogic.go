package logic

import (
	"context"

	"googee/common/utils"
	"googee/service/account/rpc/internal/svc"
	"googee/service/account/rpc/types/account"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type GetAccountLogLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetAccountLogLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetAccountLogLogic {
	return &GetAccountLogLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// 获得账户的记录
func (l *GetAccountLogLogic) GetAccountLog(in *account.AccountLogRequest) (*account.AccountLogResponse, error) {
	resp := &account.AccountLogResponse{}
	accountlog, err := l.svcCtx.AccountLogModel.FindAllEx(in.UserId, in.Current, in.Page)

	if err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to find account %v", in.UserId)
	}

	resp.Current = in.Current
	resp.Page = in.Page

	utils.MyCopier(&resp.Logs, accountlog)

	total, _ := l.svcCtx.AccountLogModel.CountEx(in.UserId)
	logx.Infof("GetAccountLog:total=%v", total)
	resp.Total = total

	return resp, nil
}
