package logic

import (
	"context"

	"googee/service/account/model"
	"googee/service/account/rpc/internal/svc"
	"googee/service/account/rpc/types/account"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/core/stores/sqlc"
)

type CreateAccountLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewCreateAccountLogic(ctx context.Context, svcCtx *svc.ServiceContext) *CreateAccountLogic {
	return &CreateAccountLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// 创建账户
func (l *CreateAccountLogic) CreateAccount(in *account.IdRequest) (resp *account.IdResponse, err error) {
	resp = &account.IdResponse{}
	accountmodel, err := l.svcCtx.AccountModel.FindOne(l.ctx, in.Id)

	if err == sqlc.ErrNotFound {
		accountmodel = &model.Account{
			Id: in.Id,
		}
		l.svcCtx.AccountModel.Insert(l.ctx, accountmodel)
	}

	copier.Copy(resp, accountmodel)

	return resp, nil
}
