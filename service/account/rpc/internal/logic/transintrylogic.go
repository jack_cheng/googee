package logic

import (
	"context"
	"math"
	"reflect"

	"googee/common/uniqueid"
	"googee/common/utils"
	"googee/service/account/model"
	"googee/service/account/rpc/internal/svc"
	"googee/service/account/rpc/types/account"

	"github.com/zeromicro/go-zero/core/logx"
	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

type TransInTryLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewTransInTryLogic(ctx context.Context, svcCtx *svc.ServiceContext) *TransInTryLogic {
	return &TransInTryLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// 转入尝试
func (l *TransInTryLogic) TransInTry(in *account.TransInRequest) (*account.Empty, error) {

	configValue := reflect.ValueOf(l.svcCtx.Config.Digital)
	precision := configValue.FieldByName(utils.FirstUpper(in.GetField())).Uint()
	// err := l.svcCtx.AccountModel.AdjustTrading(l.ctx, in.Id, in.Field, in.Amount, uint8(precision))

	t := int64(math.Pow10(int(precision)))

	err := l.svcCtx.SqlConn.TransactCtx(l.ctx, func(ctx context.Context, session sqlx.Session) error {
		err := l.svcCtx.AccountModel.AdjustTrading(ctx, in.Id, in.Field, in.Amount, uint8(precision))
		if err != nil {
			return err
		}

		_, err = l.svcCtx.AccountLogModel.Insert(ctx, &model.AccountLog{
			Id:      uniqueid.GenId(),
			Dir:     TRANIN,
			Name:    in.Field,
			TransId: in.TransId,
			Amount:  t * in.Amount,
			UserId:  in.Id,
			Typ:     in.Typ,
			Subtyp:  in.Subtyp,
			Comment: in.Comment,
		})

		return err
	})

	if err != nil {
		return nil, err
	}
	return &account.Empty{}, nil
}
