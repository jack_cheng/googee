package config

import (
	"github.com/zeromicro/go-zero/core/stores/cache"
	"github.com/zeromicro/go-zero/zrpc"
)

type Config struct {
	zrpc.RpcServerConf
	Mysql struct {
		DataSource string
	}

	CacheRedis cache.CacheConf
	Digital    struct {
		Coin   uint8
		Score  uint8
		Amount uint8
	}
}
