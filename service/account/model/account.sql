
CREATE TABLE `account` (
  `id` int NOT NULL,
  `amount` int NOT NULL DEFAULT '0' COMMENT '余额，单位为：分',
  `amount_trading` int NOT NULL DEFAULT '0' COMMENT '冻结余额 单位为：分',
  `score` int NOT NULL DEFAULT '0' COMMENT '积分',
  `score_trading` int NOT NULL DEFAULT '0' COMMENT '冻结积分',
  `coin` int NOT NULL DEFAULT '0' COMMENT '金币',
  `coin_trading` int NOT NULL DEFAULT '0' COMMENT '冻结金币',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `account_log` (
  `id` bigint NOT NULL,
  `dir` int NOT NULL DEFAULT '0' COMMENT '转入还是转出方向,1:转出，0:转入',
  `name` varchar(30) COLLATE utf8mb4_general_ci NOT NULL COMMENT '变更字段的名称',
  `trans_id` varchar(30) COLLATE utf8mb4_general_ci NOT NULL,
  `user_id` int NOT NULL COMMENT '用户ID',
  `amount` int NOT NULL DEFAULT '0' COMMENT '变更数量',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '1',
  `typ` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '类型',
  `subtyp` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '子类型',
  `comment` varchar(100) COLLATE utf8mb4_general_ci NOT NULL COMMENT '注释',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_log` (`trans_id`,`dir`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;