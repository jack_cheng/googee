package model

import (
	"context"
	"fmt"

	"github.com/Masterminds/squirrel"
	"github.com/zeromicro/go-zero/core/stores/cache"
	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

var _ TaskGroupModel = (*customTaskGroupModel)(nil)

type (
	// TaskGroupModel is an interface to be customized, add more methods here,
	// and implement the added methods in customTaskGroupModel.
	TaskGroupModel interface {
		taskGroupModel
		RowBuilder() squirrel.SelectBuilder
		Truncate(ctx context.Context)
		FindAll(ctx context.Context) ([]*TaskGroup, error)
		FindAllValideGroup(ctx context.Context, name string) ([]*TaskGroup, error)
	}

	customTaskGroupModel struct {
		*defaultTaskGroupModel
	}
)

// NewTaskGroupModel returns a model for the database table.
func NewTaskGroupModel(conn sqlx.SqlConn, c cache.CacheConf) TaskGroupModel {
	return &customTaskGroupModel{
		defaultTaskGroupModel: newTaskGroupModel(conn, c),
	}
}

func (m *defaultTaskGroupModel) RowBuilder() squirrel.SelectBuilder {
	return squirrel.Select(taskGroupRows).From(m.table)
}

func (m defaultTaskGroupModel) Truncate(ctx context.Context) {
	build := m.RowBuilder()
	query, values, err := build.ToSql()
	if err == nil {
		var resp []*TaskGroup
		err = m.QueryRowsNoCacheCtx(ctx, &resp, query, values...)
		if err == nil {
			for _, r := range resp {
				m.Delete(ctx, r.Id)
			}
			m.ExecNoCacheCtx(ctx, fmt.Sprintf("TRUNCATE %s", m.tableName()))
		}
	}
}

func (m defaultTaskGroupModel) FindAll(ctx context.Context) ([]*TaskGroup, error) {
	build := m.RowBuilder()
	query, values, err := build.ToSql()
	if err != nil {
		return nil, nil
	}

	var resp []*TaskGroup
	err = m.QueryRowsNoCacheCtx(ctx, &resp, query, values...)
	switch err {
	case nil:
		return resp, nil
	default:
		return nil, err
	}
}

func (m defaultTaskGroupModel) FindAllValideGroup(ctx context.Context, name string) ([]*TaskGroup, error) {
	build := m.RowBuilder()
	query, values, err := build.Where("valid = ? and cate=?", "Y", name).ToSql()
	if err != nil {
		return nil, nil
	}
	if err != nil {
		return nil, nil
	}

	var resp []*TaskGroup
	err = m.QueryRowsNoCacheCtx(ctx, &resp, query, values...)
	switch err {
	case nil:
		return resp, nil
	default:
		return nil, err
	}
}
