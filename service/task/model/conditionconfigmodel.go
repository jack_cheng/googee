package model

import (
	"context"
	"fmt"

	"github.com/Masterminds/squirrel"
	"github.com/zeromicro/go-zero/core/stores/cache"
	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

var _ ConditionConfigModel = (*customConditionConfigModel)(nil)

type (
	// ConditionConfigModel is an interface to be customized, add more methods here,
	// and implement the added methods in customConditionConfigModel.
	ConditionConfigModel interface {
		conditionConfigModel
		RowBuilder() squirrel.SelectBuilder
		Truncate(ctx context.Context)
		FindAll(ctx context.Context) ([]*ConditionConfig, error)
	}

	customConditionConfigModel struct {
		*defaultConditionConfigModel
	}
)

// NewConditionConfigModel returns a model for the database table.
func NewConditionConfigModel(conn sqlx.SqlConn, c cache.CacheConf) ConditionConfigModel {
	return &customConditionConfigModel{
		defaultConditionConfigModel: newConditionConfigModel(conn, c),
	}
}

func (m *defaultConditionConfigModel) RowBuilder() squirrel.SelectBuilder {
	return squirrel.Select(conditionConfigRows).From(m.table)
}

func (m defaultConditionConfigModel) Truncate(ctx context.Context) {
	build := m.RowBuilder()
	query, values, err := build.ToSql()
	if err == nil {
		var resp []*ConditionConfig
		err = m.QueryRowsNoCacheCtx(ctx, &resp, query, values...)
		if err == nil {
			for _, r := range resp {
				m.Delete(ctx, r.Id)
			}
			m.ExecNoCacheCtx(ctx, fmt.Sprintf("TRUNCATE %s", m.tableName()))
		}
	}
}

func (m defaultConditionConfigModel) FindAll(ctx context.Context) ([]*ConditionConfig, error) {
	build := m.RowBuilder()
	query, values, err := build.ToSql()
	if err != nil {
		return nil, nil
	}

	var resp []*ConditionConfig
	err = m.QueryRowsNoCacheCtx(ctx, &resp, query, values...)
	switch err {
	case nil:
		return resp, nil
	default:
		return nil, err
	}
}
