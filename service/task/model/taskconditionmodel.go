package model

import (
	"context"
	"fmt"

	"github.com/Masterminds/squirrel"
	"github.com/zeromicro/go-zero/core/stores/cache"
	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

var _ TaskConditionModel = (*customTaskConditionModel)(nil)

type (
	// TaskConditionModel is an interface to be customized, add more methods here,
	// and implement the added methods in customTaskConditionModel.
	TaskConditionModel interface {
		taskConditionModel
		RowBuilder() squirrel.SelectBuilder
		Truncate(ctx context.Context)
		FindAll(ctx context.Context) ([]*TaskCondition, error)
		FindGroupConditonsByGroupId(ctx context.Context, groupId int64) ([]*TaskCondition, error)
		FindGroupConditonsByTaskId(ctx context.Context, taskId int64) ([]*TaskCondition, error)
	}

	customTaskConditionModel struct {
		*defaultTaskConditionModel
	}
)

// NewTaskConditionModel returns a model for the database table.
func NewTaskConditionModel(conn sqlx.SqlConn, c cache.CacheConf) TaskConditionModel {
	return &customTaskConditionModel{
		defaultTaskConditionModel: newTaskConditionModel(conn, c),
	}
}

func (m *defaultTaskConditionModel) RowBuilder() squirrel.SelectBuilder {
	return squirrel.Select(taskConditionRows).From(m.table)
}

func (m defaultTaskConditionModel) Truncate(ctx context.Context) {
	build := m.RowBuilder()
	query, values, err := build.ToSql()
	if err == nil {
		var resp []*TaskCondition
		err = m.QueryRowsNoCacheCtx(ctx, &resp, query, values...)
		if err == nil {
			for _, r := range resp {
				m.Delete(ctx, r.Id)
			}
			m.ExecNoCacheCtx(ctx, fmt.Sprintf("TRUNCATE %s", m.tableName()))
		}
	}
}

func (m defaultTaskConditionModel) FindAll(ctx context.Context) ([]*TaskCondition, error) {
	build := m.RowBuilder()
	query, values, err := build.ToSql()
	if err != nil {
		return nil, nil
	}

	var resp []*TaskCondition
	err = m.QueryRowsNoCacheCtx(ctx, &resp, query, values...)
	switch err {
	case nil:
		return resp, nil
	default:
		return nil, err
	}
}

func (m defaultTaskConditionModel) FindGroupConditonsByGroupId(ctx context.Context, groupId int64) ([]*TaskCondition, error) {
	build := m.RowBuilder()
	query, values, err := build.Where(" group_id=? ", groupId).ToSql()
	if err != nil {
		return nil, nil
	}

	var resp []*TaskCondition
	err = m.QueryRowsNoCacheCtx(ctx, &resp, query, values...)
	switch err {
	case nil:
		return resp, nil
	default:
		return nil, err
	}
}

func (m defaultTaskConditionModel) FindGroupConditonsByTaskId(ctx context.Context, taskId int64) ([]*TaskCondition, error) {
	build := m.RowBuilder()
	query, values, err := build.Where(" task_id=? ", taskId).ToSql()
	if err != nil {
		return nil, nil
	}

	var resp []*TaskCondition
	err = m.QueryRowsNoCacheCtx(ctx, &resp, query, values...)
	switch err {
	case nil:
		return resp, nil
	default:
		return nil, err
	}
}
