package model

import (
	"context"
	"fmt"

	"github.com/Masterminds/squirrel"
	"github.com/zeromicro/go-zero/core/stores/cache"
	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

var _ TaskBonusModel = (*customTaskBonusModel)(nil)

type (
	// TaskBonusModel is an interface to be customized, add more methods here,
	// and implement the added methods in customTaskBonusModel.
	TaskBonusModel interface {
		taskBonusModel
		RowBuilder() squirrel.SelectBuilder
		Truncate(ctx context.Context)
		FindAll(ctx context.Context) ([]*TaskBonus, error)
		FindAllTaskBonus(ctx context.Context, taskId int64) ([]*TaskBonus, error)
	}

	customTaskBonusModel struct {
		*defaultTaskBonusModel
	}
)

// NewTaskBonusModel returns a model for the database table.
func NewTaskBonusModel(conn sqlx.SqlConn, c cache.CacheConf) TaskBonusModel {
	return &customTaskBonusModel{
		defaultTaskBonusModel: newTaskBonusModel(conn, c),
	}
}

func (m *defaultTaskBonusModel) RowBuilder() squirrel.SelectBuilder {
	return squirrel.Select(taskBonusRows).From(m.table)
}

func (m defaultTaskBonusModel) Truncate(ctx context.Context) {
	build := m.RowBuilder()
	query, values, err := build.ToSql()
	if err == nil {
		var resp []*TaskBonus
		err = m.QueryRowsNoCacheCtx(ctx, &resp, query, values...)
		if err == nil {
			for _, r := range resp {
				m.Delete(ctx, r.Id)
			}
			m.ExecNoCacheCtx(ctx, fmt.Sprintf("TRUNCATE %s", m.tableName()))
		}
	}
}

func (m defaultTaskBonusModel) FindAll(ctx context.Context) ([]*TaskBonus, error) {
	build := m.RowBuilder()
	query, values, err := build.ToSql()
	if err != nil {
		return nil, nil
	}

	var resp []*TaskBonus
	err = m.QueryRowsNoCacheCtx(ctx, &resp, query, values...)
	switch err {
	case nil:
		return resp, nil
	default:
		return nil, err
	}
}

func (m defaultTaskBonusModel) FindAllTaskBonus(ctx context.Context, taskId int64) ([]*TaskBonus, error) {
	build := m.RowBuilder()
	query, values, err := build.Where(" task_id=? ", taskId).ToSql()
	if err != nil {
		return nil, nil
	}

	var resp []*TaskBonus
	err = m.QueryRowsNoCacheCtx(ctx, &resp, query, values...)
	switch err {
	case nil:
		return resp, nil
	default:
		return nil, err
	}
}
