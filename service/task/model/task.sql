
-- 考察点的名字
CREATE TABLE `condition_config`(
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(32) NOT NULL COMMENT '条件名',
  `desc` varchar(64) NOT NULL COMMENT '条件描述',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 任务配置表
CREATE TABLE `task_group` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(124) NOT NULL,
  `valid_days` int(11) NOT NULL DEFAULT '-1' COMMENT '数据有效期(天)',
  `typ` char(1) NOT NULL DEFAULT 'N' COMMENT '是否为周期性:Y,N',
  `cron` char(20) NOT NULL DEFAULT '' COMMENT 'crontab类似的字符串，用来确定什么时候刷新',
  `desc` varchar(255) NOT NULL COMMENT '任务描述',
  `cate` varchar(255) NOT NULL COMMENT '分类名字',
  `valid` varchar(10) NOT NULL DEFAULT 'Y' COMMENT '是否启用',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 任务
CREATE TABLE `task`(
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL COMMENT '名字',
  `desc` varchar(100) NOT NULL COMMENT '任务描述',
  `group_id` int NOT NULL COMMENT '分组名字',
  `sort_index` int NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`group_id`,`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


-- 任务每一个子任务
CREATE TABLE `task_condition`(
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `task_id` int NOT NULL COMMENT '任务name',
  `task_cond_name`  varchar(20) NOT NULL COMMENT '条件名',
  `count` int NOT NULL DEFAULT '0' COMMENT '达标个数',
  `group_id` int NOT NULL COMMENT '分组ID',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_cond_unique` (`task_id`,`task_cond_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 任务奖励
CREATE TABLE `task_bonus`(
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `task_id` int NOT NULL COMMENT '任务Id',
  `name` varchar(124) COLLATE utf8mb4_general_ci NOT NULL COMMENT '奖励名字',
  `desc` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '奖励描述',
  `count` int NOT NULL COMMENT '奖励个数',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`task_id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


-- 任务状态
CREATE TABLE `task_status`(
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int NOT NULL COMMENT '分组ID',
  `task_id` int NOT NULL COMMENT '任务ID',
  `batch_id` int unsigned NOT NULL COMMENT '批号ID',
  `user_id` int NOT NULL COMMENT '关联user表的ID',
  `status` int NOT NULL DEFAULT '0' COMMENT '是否领取',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`task_id`,`batch_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- 任务进度
CREATE TABLE `task_progress` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `batch_id` int unsigned NOT NULL DEFAULT '1' COMMENT '期号',
  `group_id` int NOT NULL COMMENT '分组ID',
  `task_cond_name` varchar(20) COLLATE utf8mb4_general_ci NOT NULL COMMENT '条件名',
  `count` int NOT NULL DEFAULT '0' COMMENT '完成个数',
  `user_id` int NOT NULL COMMENT '关联user表的ID',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_batch` (`group_id`,`batch_id`,`task_cond_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
