package model

import (
	"context"
	"fmt"

	"github.com/Masterminds/squirrel"
	"github.com/zeromicro/go-zero/core/stores/cache"
	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

var _ TaskStatusModel = (*customTaskStatusModel)(nil)

type (
	// TaskStatusModel is an interface to be customized, add more methods here,
	// and implement the added methods in customTaskStatusModel.
	TaskStatusModel interface {
		taskStatusModel
		RowBuilder() squirrel.SelectBuilder
		Truncate(ctx context.Context)
		FindAll(ctx context.Context) ([]*TaskStatus, error)
		FindAllGroupTaskStatus(ctx context.Context, groupId, batch_id1, batch_id0, userId int64) ([]*TaskStatus, error)
	}

	customTaskStatusModel struct {
		*defaultTaskStatusModel
	}
)

// NewTaskStatusModel returns a model for the database table.
func NewTaskStatusModel(conn sqlx.SqlConn, c cache.CacheConf) TaskStatusModel {
	return &customTaskStatusModel{
		defaultTaskStatusModel: newTaskStatusModel(conn, c),
	}
}

func (m *defaultTaskStatusModel) RowBuilder() squirrel.SelectBuilder {
	return squirrel.Select(taskStatusRows).From(m.table)
}

func (m defaultTaskStatusModel) Truncate(ctx context.Context) {
	build := m.RowBuilder()
	query, values, err := build.ToSql()
	if err == nil {
		var resp []*TaskStatus
		err = m.QueryRowsNoCacheCtx(ctx, &resp, query, values...)
		if err == nil {
			for _, r := range resp {
				m.Delete(ctx, r.Id)
			}
			m.ExecNoCacheCtx(ctx, fmt.Sprintf("TRUNCATE %s", m.tableName()))
		}
	}
}

func (m defaultTaskStatusModel) FindAll(ctx context.Context) ([]*TaskStatus, error) {
	build := m.RowBuilder()
	query, values, err := build.ToSql()
	if err != nil {
		return nil, nil
	}

	var resp []*TaskStatus
	err = m.QueryRowsNoCacheCtx(ctx, &resp, query, values...)
	switch err {
	case nil:
		return resp, nil
	default:
		return nil, err
	}
}

func (m defaultTaskStatusModel) FindAllGroupTaskStatus(ctx context.Context, groupId int64, batchId int64,
	batchId0 int64, userId int64) ([]*TaskStatus, error) {
	build := m.RowBuilder()
	query, values, err := build.Where(" group_id=? and (batch_id=? or batch_id=?) and user_id=? and status=1", groupId, batchId, batchId0, userId).ToSql()
	if err != nil {
		return nil, nil
	}

	var resp []*TaskStatus
	err = m.QueryRowsNoCacheCtx(ctx, &resp, query, values...)
	switch err {
	case nil:
		return resp, nil
	default:
		return nil, err
	}
}
