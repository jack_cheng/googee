// Code generated by goctl. DO NOT EDIT.
package handler

import (
	"net/http"

	"googee/service/task/api/internal/svc"

	"github.com/zeromicro/go-zero/rest"
)

func RegisterHandlers(server *rest.Server, serverCtx *svc.ServiceContext) {
	server.AddRoutes(
		[]rest.Route{
			{
				Method:  http.MethodPost,
				Path:    "/task/test",
				Handler: testHandler(serverCtx),
			},
			{
				Method:  http.MethodGet,
				Path:    "/task/group/:id",
				Handler: groupHandler(serverCtx),
			},
			{
				Method:  http.MethodGet,
				Path:    "/task/require",
				Handler: requireHandler(serverCtx),
			},
		},
		rest.WithJwt(serverCtx.Config.Auth.AccessSecret),
		rest.WithPrefix("/api"),
	)

	server.AddRoutes(
		[]rest.Route{
			{
				Method:  http.MethodGet,
				Path:    "/task/group/list/:name",
				Handler: listHandler(serverCtx),
			},
		},
		rest.WithPrefix("/api"),
	)
}
