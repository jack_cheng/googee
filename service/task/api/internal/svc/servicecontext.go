package svc

import (
	"googee/service/account/rpc/account"
	"googee/service/task/api/internal/config"
	"googee/service/task/model"
	"googee/service/task/rpc/task"

	"github.com/zeromicro/go-zero/core/stores/sqlx"
	"github.com/zeromicro/go-zero/zrpc"
)

type ServiceContext struct {
	Config               config.Config
	TaskModel            model.TaskModel
	TaskBonusModel       model.TaskBonusModel
	TaskStatusModel      model.TaskStatusModel
	ConditionConfigModel model.ConditionConfigModel
	TaskProgressModel    model.TaskProgressModel
	TaskConditionModel   model.TaskConditionModel
	TaskGroupModel       model.TaskGroupModel
	SqlConn              sqlx.SqlConn
	Account              account.Account
	Task                 task.Task
}

func NewServiceContext(c config.Config) *ServiceContext {
	conn := sqlx.NewMysql(c.Mysql.DataSource)
	return &ServiceContext{
		Config:               c,
		SqlConn:              conn,
		TaskModel:            model.NewTaskModel(conn, c.CacheRedis),
		TaskBonusModel:       model.NewTaskBonusModel(conn, c.CacheRedis),
		TaskProgressModel:    model.NewTaskProgressModel(conn, c.CacheRedis),
		TaskStatusModel:      model.NewTaskStatusModel(conn, c.CacheRedis),
		ConditionConfigModel: model.NewConditionConfigModel(conn, c.CacheRedis),
		TaskConditionModel:   model.NewTaskConditionModel(conn, c.CacheRedis),
		TaskGroupModel:       model.NewTaskGroupModel(conn, c.CacheRedis),
		Account:              account.NewAccount(zrpc.MustNewClient(c.Account)),
		Task:                 task.NewTask(zrpc.MustNewClient(c.Task)),
	}
}
