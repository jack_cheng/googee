package logic

type X struct {
	TaskId   int64  `db:"task_id"`   // 任务ID
	Count    int64  `db:"count"`     //
	CondName string `db:"cond_name"` //
	BatchId  int64  `db:"batch_id"`  //期号
}
