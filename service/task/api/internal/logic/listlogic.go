package logic

import (
	"context"

	"googee/common/errorx"
	"googee/service/task/api/internal/svc"
	"googee/service/task/api/internal/types"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
)

type ListLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *ListLogic {
	return &ListLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *ListLogic) List(req *types.CateReq) (resp *types.CateListReply, err error) {
	resp = &types.CateListReply{}

	groups, err := l.svcCtx.TaskGroupModel.FindAllValideGroup(l.ctx, req.Name)
	if err != nil {
		return nil, errorx.NewCodeErrorNoMsg(errorx.INTERNAL_ERROR)
	}

	copier.Copy(&resp.Data.CateList, groups)
	return resp, nil
}
