// Code generated by goctl. DO NOT EDIT!
// Source: task.proto

package task

import (
	"context"

	"googee/service/task/rpc/types/task"

	"github.com/zeromicro/go-zero/zrpc"
	"google.golang.org/grpc"
)

type (
	IdRequest         = task.IdRequest
	TaskBonus         = task.TaskBonus
	TaskCondition     = task.TaskCondition
	TaskGroupResponse = task.TaskGroupResponse
	TaskInfo          = task.TaskInfo

	Task interface {
		// 获得一个任务组的完成状态
		GetTaskGroup(ctx context.Context, in *IdRequest, opts ...grpc.CallOption) (*TaskGroupResponse, error)
	}

	defaultTask struct {
		cli zrpc.Client
	}
)

func NewTask(cli zrpc.Client) Task {
	return &defaultTask{
		cli: cli,
	}
}

// 获得一个任务组的完成状态
func (m *defaultTask) GetTaskGroup(ctx context.Context, in *IdRequest, opts ...grpc.CallOption) (*TaskGroupResponse, error) {
	client := task.NewTaskClient(m.cli.Conn())
	return client.GetTaskGroup(ctx, in, opts...)
}
