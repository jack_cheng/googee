package logic

import (
	"context"

	"googee/service/task/rpc/internal/svc"
	"googee/service/task/rpc/types/task"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type GetTaskGroupLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetTaskGroupLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetTaskGroupLogic {
	return &GetTaskGroupLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// 获得一个任务组的完成状态
func (l *GetTaskGroupLogic) GetTaskGroup(in *task.IdRequest) (*task.TaskGroupResponse, error) {
	// todo: add your logic here and delete this line
	_, err := l.svcCtx.TaskGroupModel.FindOne(l.ctx, in.Id)
	if err != nil {
		return nil, status.Error(codes.NotFound, "id was not found")
	}

	//查询group对应的所有的任务状态

	return &task.TaskGroupResponse{}, nil
}
