package svc

import (
	"googee/service/task/model"
	"googee/service/task/rpc/internal/config"

	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

type ServiceContext struct {
	Config               config.Config
	TaskModel            model.TaskModel
	TaskBonusModel       model.TaskBonusModel
	TaskStatusModel      model.TaskStatusModel
	ConditionConfigModel model.ConditionConfigModel
	TaskProgressModel    model.TaskProgressModel
	TaskConditionModel   model.TaskConditionModel
	TaskGroupModel       model.TaskGroupModel
}

func NewServiceContext(c config.Config) *ServiceContext {
	conn := sqlx.NewMysql(c.Mysql.DataSource)
	return &ServiceContext{
		Config:               c,
		TaskModel:            model.NewTaskModel(conn, c.CacheRedis),
		TaskBonusModel:       model.NewTaskBonusModel(conn, c.CacheRedis),
		TaskProgressModel:    model.NewTaskProgressModel(conn, c.CacheRedis),
		TaskStatusModel:      model.NewTaskStatusModel(conn, c.CacheRedis),
		ConditionConfigModel: model.NewConditionConfigModel(conn, c.CacheRedis),
		TaskConditionModel:   model.NewTaskConditionModel(conn, c.CacheRedis),
		TaskGroupModel:       model.NewTaskGroupModel(conn, c.CacheRedis),
	}
}
