// Code generated by goctl. DO NOT EDIT.
package types

type SearchReq struct {
	Category string `json:"category"`
	Content  string `json:"content"`
	Offset   int64  `json:"offset"`
	Limit    int64  `json:"limit"`
}

type SearchReplyData struct {
	Hits   []interface{} `json:"hits"`
	Offset int64         `json:"offset"`
	Limit  int64         `json:"limit"`
}

type SearchReply struct {
	Code int             `json:"code"`
	Msg  string          `json:"msg"`
	Data SearchReplyData `json:"data"`
}
