package config

import "github.com/zeromicro/go-zero/rest"

type Config struct {
	rest.RestConf
	MeiliSearchServer struct {
		Host   string
		APIKey string
	}
}
