package svc

import (
	"googee/service/search/api/internal/config"

	"github.com/meilisearch/meilisearch-go"
	"github.com/zeromicro/go-zero/core/logx"
)

type ServiceContext struct {
	Config      config.Config
	MeiliClient *meilisearch.Client
}

func NewServiceContext(c config.Config) *ServiceContext {
	logx.Infof("美丽搜索 Host=%v", c.MeiliSearchServer.Host)
	logx.Infof("美丽搜索 Master Key=%v", c.MeiliSearchServer.APIKey)
	client := meilisearch.NewClient(meilisearch.ClientConfig{
		Host:   c.MeiliSearchServer.Host,
		APIKey: c.MeiliSearchServer.APIKey,
	})
	return &ServiceContext{
		Config:      c,
		MeiliClient: client,
	}
}
