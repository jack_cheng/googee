
CREATE TABLE `tb_attribute_key`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '编号',
  `attribute_name` varchar(22) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属性名',
  `priority` int(0) UNSIGNED NULL DEFAULT NULL COMMENT '优先级',
  `category_id` int(0) UNSIGNED NULL DEFAULT NULL COMMENT '类别编号',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `category_id`(`category_id`) USING BTREE,
  CONSTRAINT `tb_attribute_key_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `tb_product_category` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '属性key表' ROW_FORMAT = Dynamic;


CREATE TABLE `tb_attribute_value`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '编号',
  `attribute_id` int(0) UNSIGNED NULL DEFAULT NULL COMMENT '属性编号',
  `priority` int(0) UNSIGNED NULL DEFAULT NULL COMMENT '优先级',
  `attribute_value` varchar(22) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '属性值',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `category_id`(`attribute_id`) USING BTREE,
  CONSTRAINT `tb_attribute_value_ibfk_1` FOREIGN KEY (`attribute_id`) REFERENCES `tb_attribute_key` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '属性value表' ROW_FORMAT = Dynamic;


CREATE TABLE `tb_brand`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '品牌编号',
  `name` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌名称',
  `logo` varchar(180) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌LOGO',
  `info` varchar(220) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌描述',
  `status` tinyint(0) UNSIGNED NULL DEFAULT 1 COMMENT '状态 1上架  2下架',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '品牌表' ROW_FORMAT = Dynamic;

CREATE TABLE `tb_product`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '商品编号',
  `name` varchar(28) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '商品名称',
  `attribute_list` json NULL COMMENT '商品属性列表',
  `category_id` int(0) UNSIGNED NULL DEFAULT NULL COMMENT '类别编号',
  `brand_id` int(0) UNSIGNED NULL DEFAULT NULL COMMENT '品牌编号',
  `status` tinyint(0) UNSIGNED NULL DEFAULT NULL COMMENT '状态 1上架  2下架',
  `info` varchar(180) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT ' 创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT ' 更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `category_id`(`category_id`) USING BTREE,
  INDEX `brand_id`(`brand_id`) USING BTREE,
  CONSTRAINT `tb_product_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `tb_product_category` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `tb_product_ibfk_2` FOREIGN KEY (`brand_id`) REFERENCES `tb_brand` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品表（SPU）' ROW_FORMAT = Dynamic;


CREATE TABLE `tb_product_category`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '类别编号',
  `name` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '类别名称',
  `status` tinyint(0) UNSIGNED NULL DEFAULT 1 COMMENT '状态：1上架 2下架',
  `priority` int(0) UNSIGNED NULL DEFAULT NULL COMMENT '优先级',
  `pid` int(0) UNSIGNED NULL DEFAULT NULL COMMENT '父类别编号',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `pid`(`pid`) USING BTREE,
  CONSTRAINT `tb_product_category_ibfk_1` FOREIGN KEY (`pid`) REFERENCES `tb_product_category` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品类别表' ROW_FORMAT = Dynamic;


CREATE TABLE `tb_product_specs`  (
  `id` int(0) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '规格编号',
  `product_id` int(0) UNSIGNED NULL DEFAULT NULL COMMENT '商品编号',
  `name` varchar(24) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规格名称',
  `sell_point` varchar(48) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '卖点',
  `product_specs` json NULL COMMENT '规格列表',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '售价',
  `barcode` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '条形码',
  `img` varchar(120) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '首图',
  `pics` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '图片列表',
  `amount` int(0) UNSIGNED NULL DEFAULT NULL COMMENT '库存',
  `status` tinyint(0) UNSIGNED NULL DEFAULT 1 COMMENT '状态 1上架  2下架',
  `info` varchar(180) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `create_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `update_time` datetime(0) NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `product_id`(`product_id`) USING BTREE,
  CONSTRAINT `tb_product_specs_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `tb_product` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '商品规格表（sku表）' ROW_FORMAT = Dynamic;
