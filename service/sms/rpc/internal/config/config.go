package config

import (
	"github.com/zeromicro/go-zero/core/stores/cache"
	"github.com/zeromicro/go-zero/zrpc"
)

type Config struct {
	zrpc.RpcServerConf
	Mysql struct {
		DataSource string
	}

	VerifySMS struct {
		//发送验证短信的时间间隔
		Interval int64
		//短信有效时间
		Valid int64
	}
	CacheRedis cache.CacheConf
}
