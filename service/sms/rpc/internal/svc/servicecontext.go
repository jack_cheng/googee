package svc

import (
	"googee/service/sms/model"
	"googee/service/sms/rpc/internal/config"

	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

type ServiceContext struct {
	Config   config.Config
	SmsModel model.SmsModel
}

func NewServiceContext(c config.Config) *ServiceContext {
	conn := sqlx.NewMysql(c.Mysql.DataSource)
	return &ServiceContext{
		Config:   c,
		SmsModel: model.NewSmsModel(conn, c.CacheRedis),
	}
}
