CREATE TABLE `sms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(20) DEFAULT NULL COMMENT '电话',
  `user_id` int(11) DEFAULT NULL COMMENT '关联user表的ID',
  `typ` TINYINT(1) DEFAULT '0' COMMENT '短信类型',
  `status` TINYINT(1) DEFAULT '0' COMMENT '状态:1:已发送,0:未发送',
  `valid` TINYINT(1) DEFAULT '1' COMMENT '状态:1:有效,0:无效',
  `content` varchar(300) DEFAULT NULL COMMENT '短信内容',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `send_time` datetime DEFAULT NULL COMMENT '短信发送时间' ,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
