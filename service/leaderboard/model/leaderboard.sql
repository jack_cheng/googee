-- 排行榜的主表


CREATE TABLE `leaderboards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(124) NOT NULL,
  `valid_days` int(11) NOT NULL DEFAULT '-1' COMMENT '数据有效期(天)',
  `typ` char(1) NOT NULL DEFAULT 'N' COMMENT '是否为周期性:Y,N',
  `cron` char(20) NOT NULL DEFAULT '' COMMENT 'crontab类似的字符串，用来确定什么时候刷新',
  `desc` varchar(255) NOT NULL COMMENT '排行榜描述',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `leaderboards_buckets` (
  `id` bigint NOT NULL,
  `bid` int NOT NULL,
  `batchid` bigint NOT NULL DEFAULT '1' COMMENT '批次ID',
  `from_score` int NOT NULL,
  `to_score` int NOT NULL,
  `from_rank` int NOT NULL,
  `to_rank` int NOT NULL,
  `from_dense` int NOT NULL,
  `to_dense` int NOT NULL,
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bid` (`bid`,`batchid`,`from_score`,`to_score`),
  UNIQUE KEY `leaderboards_sort_score` (`bid`,`batchid`,`from_score`,`to_score`),
  UNIQUE KEY `	leaderboards_sort_dense` (`bid`,`batchid`,`from_dense`,`to_dense`),
  KEY `leaderboards_sort_rank` (`bid`,`batchid`,`from_dense`,`to_rank`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

CREATE TABLE `leaderboards_data` (
  `id` bigint NOT NULL COMMENT '唯一值',
  `bid` int unsigned NOT NULL COMMENT '排行榜ID',
  `batchid` bigint NOT NULL DEFAULT '1' COMMENT '批次ID',
  `user_id` int NOT NULL COMMENT '用户ID',
  `score` int unsigned NOT NULL DEFAULT '0',
  `data` varchar(1024) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '扩展数据',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY (`bid`,`batchid`,`user_id`),
  KEY `leaderboards_data` (`bid`,`batchid`,`score`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;