package model

import (
	"context"
	"fmt"

	"github.com/Masterminds/squirrel"
	"github.com/zeromicro/go-zero/core/stores/cache"
	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

var _ LeaderboardsDataModel = (*customLeaderboardsDataModel)(nil)

type (
	// LeaderboardsDataModel is an interface to be customized, add more methods here,
	// and implement the added methods in customLeaderboardsDataModel.
	LeaderboardsDataModel interface {
		leaderboardsDataModel
		RowBuilder() squirrel.SelectBuilder
		Truncate(ctx context.Context)
	}

	customLeaderboardsDataModel struct {
		*defaultLeaderboardsDataModel
	}
)

// NewLeaderboardsDataModel returns a model for the database table.
func NewLeaderboardsDataModel(conn sqlx.SqlConn, c cache.CacheConf) LeaderboardsDataModel {
	return &customLeaderboardsDataModel{
		defaultLeaderboardsDataModel: newLeaderboardsDataModel(conn, c),
	}
}

func (m *defaultLeaderboardsDataModel) RowBuilder() squirrel.SelectBuilder {
	return squirrel.Select(leaderboardsDataRows).From(m.table)
}

func (m defaultLeaderboardsDataModel) Truncate(ctx context.Context) {
	build := m.RowBuilder()
	query, values, err := build.ToSql()
	if err == nil {
		var resp []*LeaderboardsData
		err = m.QueryRowsNoCacheCtx(ctx, &resp, query, values...)
		if err == nil {
			for _, r := range resp {
				m.Delete(ctx, r.Id)
			}
			m.ExecNoCacheCtx(ctx, fmt.Sprintf("TRUNCATE %s", m.tableName()))
		}
	}
}
