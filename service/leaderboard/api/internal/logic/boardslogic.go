package logic

import (
	"context"

	"googee/common/utils"
	"googee/service/leaderboard/api/internal/svc"
	"googee/service/leaderboard/api/internal/types"
	"googee/service/leaderboard/rpc/leaderboard"

	"github.com/zeromicro/go-zero/core/logx"
)

type BoardsLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewBoardsLogic(ctx context.Context, svcCtx *svc.ServiceContext) *BoardsLogic {
	return &BoardsLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *BoardsLogic) Boards(req *types.AllBoardReq) (resp *types.AllBoardReply, err error) {
	resp = &types.AllBoardReply{}

	result, err := l.svcCtx.Leaderboard.GetAllBoard(l.ctx, &leaderboard.AllBoardRequest{})

	utils.MyCopier(&resp.Data.Boards, result.Boards)

	return
}
