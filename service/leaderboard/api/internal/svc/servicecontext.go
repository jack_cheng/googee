package svc

import (
	"googee/service/leaderboard/api/internal/config"
	"googee/service/leaderboard/rpc/leaderboard"
	"googee/service/user/rpc/user"

	"github.com/zeromicro/go-zero/zrpc"
)

type ServiceContext struct {
	Config      config.Config
	Leaderboard leaderboard.Leaderboard
	User        user.User
}

func NewServiceContext(c config.Config) *ServiceContext {
	return &ServiceContext{
		Config:      c,
		Leaderboard: leaderboard.NewLeaderboard(zrpc.MustNewClient(c.Leaderboard)),
		User:        user.NewUser(zrpc.MustNewClient(c.User)),
	}
}
