package handler

import (
	"net/http"

	"github.com/zeromicro/go-zero/rest/httpx"

	"googee/common/errorx"

	"github.com/go-playground/validator"

	"googee/service/leaderboard/api/internal/logic"
	"googee/service/leaderboard/api/internal/svc"
	"googee/service/leaderboard/api/internal/types"
)

func ranklistHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.RankListReq
		if err := httpx.Parse(r, &req); err != nil {
			httpx.Error(w, errorx.NewCodeError(3, err.Error()))
			return
		}

		if err := validator.New().StructCtx(r.Context(), req); err != nil {
			httpx.Error(w, errorx.NewCodeError(3, err.Error()))
			return
		}

		l := logic.NewRanklistLogic(r.Context(), svcCtx)
		resp, err := l.Ranklist(&req)
		if err != nil {
			httpx.Error(w, err)
		} else {
			httpx.OkJson(w, resp)
		}
	}
}
