package logic

import (
	"context"

	"googee/service/leaderboard/rpc/internal/svc"
	"googee/service/leaderboard/rpc/types/leaderboard"

	"github.com/fatih/color"
	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type GetAllBoardLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetAllBoardLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetAllBoardLogic {
	return &GetAllBoardLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// 获取排行榜的分类
func (l *GetAllBoardLogic) GetAllBoard(in *leaderboard.AllBoardRequest) (*leaderboard.AllBoardResponse, error) {
	boards, err := l.svcCtx.LeaderboardsModel.FindAll(l.ctx)

	if err != nil {
		return nil, status.Error(codes.Internal, "interval error")
	}

	for i, b := range boards {
		color.Red("i=%v", i)
		color.Red("b=%v", b.Name)
	}

	resp := &leaderboard.AllBoardResponse{}
	copier.Copy(&resp.Boards, &boards)
	return resp, nil
}
