package logic

import (
	"context"

	"googee/service/leaderboard/rpc/internal/svc"
	"googee/service/leaderboard/rpc/types/leaderboard"

	"github.com/zeromicro/go-zero/core/logx"
)

type AddScoreLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewAddScoreLogic(ctx context.Context, svcCtx *svc.ServiceContext) *AddScoreLogic {
	return &AddScoreLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// 增加积分
func (l *AddScoreLogic) AddScore(in *leaderboard.AddScoreRequest) (*leaderboard.AddScoreResponse, error) {
	// todo: add your logic here and delete this line

	// TODO: 检查排行榜是否存在

	// TODO: 增加积分

	return &leaderboard.AddScoreResponse{}, nil
}
