package logic

import (
	"context"

	"googee/service/leaderboard/rpc/internal/svc"
	"googee/service/leaderboard/rpc/types/leaderboard"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetByNameLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetByNameLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetByNameLogic {
	return &GetByNameLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// 根据名字获得当个排行榜的定义信息
func (l *GetByNameLogic) GetByName(in *leaderboard.NameRequest) (*leaderboard.NameResponse, error) {
	// todo: add your logic here and delete this line

	return &leaderboard.NameResponse{}, nil
}
