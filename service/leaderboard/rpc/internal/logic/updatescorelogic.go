package logic

import (
	"context"

	"googee/service/leaderboard/rpc/internal/svc"
	"googee/service/leaderboard/rpc/types/leaderboard"

	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateScoreLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpdateScoreLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpdateScoreLogic {
	return &UpdateScoreLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// 更新积分
func (l *UpdateScoreLogic) UpdateScore(in *leaderboard.UpdateScoreRequest) (*leaderboard.UpdateScoreResponse, error) {
	// todo: add your logic here and delete this line

	return &leaderboard.UpdateScoreResponse{}, nil
}
