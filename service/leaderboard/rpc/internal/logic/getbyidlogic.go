package logic

import (
	"context"

	"googee/service/leaderboard/rpc/internal/svc"
	"googee/service/leaderboard/rpc/types/leaderboard"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetByIdLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetByIdLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetByIdLogic {
	return &GetByIdLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// 根据ID获得当个的排行榜的信息
func (l *GetByIdLogic) GetById(in *leaderboard.IdRequest) (*leaderboard.IdResponse, error) {
	// todo: add your logic here and delete this line

	return &leaderboard.IdResponse{}, nil
}
