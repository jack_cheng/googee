package svc

import (
	"googee/service/addrbook/api/internal/config"
	"googee/service/addrbook/rpc/addrbook"

	"github.com/zeromicro/go-zero/zrpc"
)

type ServiceContext struct {
	Config   config.Config
	AddrBook addrbook.AddrBook
}

func NewServiceContext(c config.Config) *ServiceContext {
	return &ServiceContext{
		Config:   c,
		AddrBook: addrbook.NewAddrBook(zrpc.MustNewClient(c.Addrbook)),
	}
}
