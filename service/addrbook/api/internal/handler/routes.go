// Code generated by goctl. DO NOT EDIT.
package handler

import (
	"net/http"

	"googee/service/addrbook/api/internal/svc"

	"github.com/zeromicro/go-zero/rest"
)

func RegisterHandlers(server *rest.Server, serverCtx *svc.ServiceContext) {
	server.AddRoutes(
		[]rest.Route{
			{
				Method:  http.MethodGet,
				Path:    "/addrbook/list",
				Handler: booklistHandler(serverCtx),
			},
			{
				Method:  http.MethodGet,
				Path:    "/addrbook/del/:id",
				Handler: delHandler(serverCtx),
			},
			{
				Method:  http.MethodGet,
				Path:    "/addrbook/:id",
				Handler: getHandler(serverCtx),
			},
			{
				Method:  http.MethodGet,
				Path:    "/addrbook/default/:id",
				Handler: defaultHandler(serverCtx),
			},
			{
				Method:  http.MethodPost,
				Path:    "/addrbook/create",
				Handler: createHandler(serverCtx),
			},
		},
		rest.WithJwt(serverCtx.Config.Auth.AccessSecret),
		rest.WithPrefix("/api"),
	)
}
