package logic

import (
	"context"
	"encoding/json"

	"googee/common/errorx"
	"googee/service/addrbook/api/internal/svc"
	"googee/service/addrbook/api/internal/types"
	"googee/service/addrbook/rpc/addrbook"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
)

type DelLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewDelLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DelLogic {
	return &DelLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *DelLogic) Del(req *types.IdPathReq) (resp *types.BookListReply, err error) {
	id, err := l.ctx.Value("userId").(json.Number).Int64()
	if err != nil {
		return nil, err
	}

	resp = &types.BookListReply{}
	resp.Code = 0

	result, err := l.svcCtx.AddrBook.DelAddrBook(l.ctx, &addrbook.AddrIdRequest{
		Id:     int64(req.Id),
		UserId: id,
	})

	if err != nil {
		return nil, errorx.NewCodeErrorNoMsg(errorx.ADDRBOOK_ERROR + 1)
	}

	if result != nil {
		copier.Copy(&resp.Data.BookList, result.Booklist)
	}

	return
}
