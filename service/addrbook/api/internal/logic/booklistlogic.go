package logic

import (
	"context"
	"encoding/json"

	"googee/service/addrbook/api/internal/svc"
	"googee/service/addrbook/api/internal/types"
	"googee/service/addrbook/rpc/addrbook"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
)

type BooklistLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewBooklistLogic(ctx context.Context, svcCtx *svc.ServiceContext) *BooklistLogic {
	return &BooklistLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *BooklistLogic) Booklist(req *types.BookListReq) (resp *types.BookListReply, err error) {

	id, err := l.ctx.Value("userId").(json.Number).Int64()
	if err != nil {
		return nil, err
	}

	resp = &types.BookListReply{}
	resp.Code = 0

	result, _ := l.svcCtx.AddrBook.GetUserAddrBookList(l.ctx, &addrbook.IdRequest{
		Id: id,
	})

	if result != nil {
		copier.Copy(&resp.Data.BookList, result.Booklist)
	}

	return
}
