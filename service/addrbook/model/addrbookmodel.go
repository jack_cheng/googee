package model

import (
	"context"
	"fmt"

	"github.com/Masterminds/squirrel"
	"github.com/zeromicro/go-zero/core/stores/cache"
	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

var _ AddrbookModel = (*customAddrbookModel)(nil)

type (
	// AddrbookModel is an interface to be customized, add more methods here,
	// and implement the added methods in customAddrbookModel.
	AddrbookModel interface {
		addrbookModel
		RowBuilder() squirrel.SelectBuilder
		Truncate(ctx context.Context)
		FindAll(ctx context.Context) ([]*Addrbook, error)
		FindAllByUserId(ctx context.Context, id int64) ([]*Addrbook, error)
	}

	customAddrbookModel struct {
		*defaultAddrbookModel
	}
)

// NewAddrbookModel returns a model for the database table.
func NewAddrbookModel(conn sqlx.SqlConn, c cache.CacheConf) AddrbookModel {
	return &customAddrbookModel{
		defaultAddrbookModel: newAddrbookModel(conn, c),
	}
}

func (m *defaultAddrbookModel) RowBuilder() squirrel.SelectBuilder {
	return squirrel.Select(addrbookRows).From(m.table)
}

func (m defaultAddrbookModel) Truncate(ctx context.Context) {
	build := m.RowBuilder()
	query, values, err := build.ToSql()
	if err == nil {
		var resp []*Addrbook
		err = m.QueryRowsNoCacheCtx(ctx, &resp, query, values...)
		if err == nil {
			for _, r := range resp {
				m.Delete(ctx, r.Id)
			}
			m.ExecNoCacheCtx(ctx, fmt.Sprintf("TRUNCATE %s", m.tableName()))
		}
	}
}

func (m defaultAddrbookModel) FindAll(ctx context.Context) ([]*Addrbook, error) {
	build := m.RowBuilder()
	query, values, err := build.ToSql()
	if err != nil {
		return nil, nil
	}

	var resp []*Addrbook
	err = m.QueryRowsNoCacheCtx(ctx, &resp, query, values...)
	switch err {
	case nil:
		return resp, nil
	default:
		return nil, err
	}
}

func (m defaultAddrbookModel) FindAllByUserId(ctx context.Context, id int64) ([]*Addrbook, error) {
	build := m.RowBuilder()
	query, values, err := build.Where("user_id = ?", id).ToSql()
	if err != nil {
		return nil, nil
	}

	var resp []*Addrbook
	err = m.QueryRowsNoCacheCtx(ctx, &resp, query, values...)
	// color.Red("resp=%v", resp)
	switch err {
	case nil:
		return resp, nil
	default:
		return nil, err
	}
}
