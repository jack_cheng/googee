package logic

import (
	"context"

	"googee/service/addrbook/rpc/addrbook"
	"googee/service/addrbook/rpc/internal/svc"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type GetUserAddrBookLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetUserAddrBookLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetUserAddrBookLogic {
	return &GetUserAddrBookLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// 获得单个地址的信息
func (l *GetUserAddrBookLogic) GetUserAddrBook(in *addrbook.AddrIdRequest) (resp *addrbook.AddrBookInfo, err error) {
	resp = &addrbook.AddrBookInfo{}
	book, err := l.svcCtx.AddrbookModel.FindOneByIdUserId(l.ctx, in.Id, in.UserId)
	if book == nil || err != nil {
		return nil, status.Error(codes.NotFound, "addrbook not found")
	}

	copier.Copy(&resp, book)
	return resp, nil
}
