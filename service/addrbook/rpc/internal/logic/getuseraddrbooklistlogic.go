package logic

import (
	"context"

	"googee/service/addrbook/rpc/addrbook"
	"googee/service/addrbook/rpc/internal/svc"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetUserAddrBookListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetUserAddrBookListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetUserAddrBookListLogic {
	return &GetUserAddrBookListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// 获得用户地址列表
func (l *GetUserAddrBookListLogic) GetUserAddrBookList(in *addrbook.IdRequest) (*addrbook.BookListResponse, error) {
	resp := &addrbook.BookListResponse{}

	books, _ := l.svcCtx.AddrbookModel.FindAllByUserId(l.ctx, in.Id)
	if books != nil {
		copier.Copy(&resp.Booklist, books)
	}

	return resp, nil
}
