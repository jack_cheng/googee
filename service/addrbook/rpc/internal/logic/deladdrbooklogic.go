package logic

import (
	"context"

	"googee/service/addrbook/rpc/addrbook"
	"googee/service/addrbook/rpc/internal/svc"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type DelAddrBookLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewDelAddrBookLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DelAddrBookLogic {
	return &DelAddrBookLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// 删除地址
func (l *DelAddrBookLogic) DelAddrBook(in *addrbook.AddrIdRequest) (resp *addrbook.BookListResponse, err error) {
	resp = &addrbook.BookListResponse{}

	book, err := l.svcCtx.AddrbookModel.FindOneByIdUserId(l.ctx, in.Id, in.UserId)
	if book != nil {
		l.svcCtx.AddrbookModel.Delete(l.ctx, book.Id)
	} else {
		return nil, status.Error(codes.NotFound, "addrbook not found")
	}

	books, _ := l.svcCtx.AddrbookModel.FindAllByUserId(l.ctx, in.Id)
	if books != nil {
		copier.Copy(&resp.Booklist, books)
	}
	return resp, err
}
