package svc

import (
	"googee/service/addrbook/model"
	"googee/service/addrbook/rpc/internal/config"

	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

type ServiceContext struct {
	Config        config.Config
	AddrbookModel model.AddrbookModel
	SqlConn       sqlx.SqlConn
}

func NewServiceContext(c config.Config) *ServiceContext {
	conn := sqlx.NewMysql(c.Mysql.DataSource)
	return &ServiceContext{
		Config:        c,
		AddrbookModel: model.NewAddrbookModel(conn, c.CacheRedis),
		SqlConn:       conn,
	}
}
