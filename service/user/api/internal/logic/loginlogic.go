package logic

import (
	"context"
	"time"

	"googee/common/errorx"
	"googee/common/utils"
	"googee/service/user/api/internal/svc"
	"googee/service/user/api/internal/types"
	"googee/service/user/rpc/user"

	"github.com/fatih/color"
	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type LoginLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewLoginLogic(ctx context.Context, svcCtx *svc.ServiceContext) *LoginLogic {
	return &LoginLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *LoginLogic) Login(req *types.LoginReq) (resp *types.LoginReply, err error) {

	name := user.NameRequest{Name: req.Username}
	u, err := l.svcCtx.User.GetUserByName(l.ctx, &name)
	if err != nil {
		st, ok := status.FromError(err)
		if ok {
			if st.Code() == codes.NotFound {
				return nil, errorx.NewCodeErrorNoMsg(errorx.USER_ERROR + 1)
			} else if st.Code() == codes.Internal {
				return nil, errorx.NewCodeError(errorx.INTERNAL_ERROR, "系统内部未知错误")
			}
		} else {
			return nil, errorx.NewCodeError(errorx.INTERNAL_ERROR, "系统内部未知错误")
		}
	}

	if u.Isderegister == 1 {
		return nil, errorx.NewCodeErrorNoMsg(errorx.USER_ERROR + 7)
	}

	if u.Password != req.Password {
		return nil, errorx.NewCodeErrorNoMsg(errorx.USER_ERROR + 1)
	}

	resp = &types.LoginReply{}
	_ = copier.Copy(&resp.Data.User, u)

	now := time.Now().Unix()
	accessExpire := l.svcCtx.Config.Auth.AccessExpire

	jwt, err := utils.GetJwtToken(l.svcCtx.Config.Auth.AccessSecret, now, accessExpire, u.Id)
	color.Red("jwt=%v", jwt)
	if err != nil {
		color.Red("err:%v", err)
	}
	resp.Data.JWT.AccessExpire = now + accessExpire
	resp.Data.JWT.RefreshAfter = now + accessExpire/2
	resp.Data.JWT.AccessToken = jwt

	return resp, nil
}
