package logic

import (
	"context"
	"encoding/json"

	"googee/common/errorx"
	"googee/service/user/api/internal/svc"
	"googee/service/user/api/internal/types"
	"googee/service/user/rpc/user"

	"github.com/zeromicro/go-zero/core/logx"
)

type DeregisterLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewDeregisterLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DeregisterLogic {
	return &DeregisterLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *DeregisterLogic) Deregister() (resp *types.BaseReply, err error) {

	resp = &types.BaseReply{}
	resp.Msg = "注销成功"

	id, err := l.ctx.Value("userId").(json.Number).Int64()
	if err != nil {
		return nil, err
	}

	if err != nil {
		return nil, err
	}
	_, err = l.svcCtx.User.Deregister(l.ctx, &user.DeregisterRequest{Id: id})
	if err != nil {
		return nil, errorx.NewCodeErrorNoMsg(errorx.USER_ERROR + 6)
	}
	return
}
