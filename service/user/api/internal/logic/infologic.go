package logic

import (
	"context"

	"googee/common/errorx"
	"googee/service/user/api/internal/svc"
	"googee/service/user/api/internal/types"
	"googee/service/user/rpc/types/user"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type InfoLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewInfoLogic(ctx context.Context, svcCtx *svc.ServiceContext) *InfoLogic {
	return &InfoLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *InfoLogic) Info(req *types.UserInfoReq) (resp *types.UserInfoReply, err error) {
	// todo: add your logic here and delete this line
	logx.Infof("userId: %v", l.ctx.Value("userId"))

	u, err := l.svcCtx.User.GetUser(l.ctx, &user.IdRequest{Id: int64(req.Id)})
	if err != nil {
		st, ok := status.FromError(err)
		if ok {
			if st.Code() == codes.NotFound {
				return nil, errorx.NewCodeErrorNoMsg(errorx.USER_ERROR + 1)
			} else if st.Code() == codes.Unavailable {
				return nil, errorx.NewCodeError(errorx.INTERNAL_ERROR, "系统内部未知错误")
			}
		} else {
			return nil, errorx.NewCodeError(errorx.INTERNAL_ERROR, "系统内部未知错误")
		}
	}

	resp = &types.UserInfoReply{}
	_ = copier.Copy(&resp.Data, u)

	if req.Id != l.ctx.Value("userId") {
		resp.Data.Phone = ""
	}

	return
}
