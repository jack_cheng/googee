package config

import (
	"github.com/zeromicro/go-zero/rest"
	"github.com/zeromicro/go-zero/zrpc"
)

type Config struct {
	rest.RestConf
	User    zrpc.RpcClientConf
	Captcha zrpc.RpcClientConf
	Account zrpc.RpcClientConf
	Sms     zrpc.RpcClientConf
	Auth    struct {
		AccessSecret string
		AccessExpire int64
	}
	DtmServer string
}
