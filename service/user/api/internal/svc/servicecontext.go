package svc

import (
	"googee/service/account/rpc/account"
	"googee/service/captcha/rpc/captcha"
	"googee/service/sms/rpc/sms"
	"googee/service/user/api/internal/config"
	"googee/service/user/rpc/user"

	"github.com/zeromicro/go-zero/zrpc"
)

type ServiceContext struct {
	Config  config.Config
	User    user.User
	Account account.Account
	Captcha captcha.Captcha
	Sms     sms.Sms
}

func NewServiceContext(c config.Config) *ServiceContext {
	return &ServiceContext{
		Config:  c,
		User:    user.NewUser(zrpc.MustNewClient(c.User)),
		Captcha: captcha.NewCaptcha(zrpc.MustNewClient(c.Captcha)),
		Account: account.NewAccount(zrpc.MustNewClient(c.Account)),
		Sms:     sms.NewSms(zrpc.MustNewClient(c.Sms)),
	}
}
