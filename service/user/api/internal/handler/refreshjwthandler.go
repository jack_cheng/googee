package handler

import (
	"net/http"

	"github.com/zeromicro/go-zero/rest/httpx"

	"googee/service/user/api/internal/logic"
	"googee/service/user/api/internal/svc"
)

func refreshjwtHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		l := logic.NewRefreshjwtLogic(r.Context(), svcCtx)
		resp, err := l.Refreshjwt()
		if err != nil {
			httpx.Error(w, err)
		} else {
			httpx.OkJson(w, resp)
		}
	}
}
