package handler

import (
	"net/http"

	"github.com/zeromicro/go-zero/rest/httpx"

	"googee/service/user/api/internal/logic"
	"googee/service/user/api/internal/svc"
)

func deregisterHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		l := logic.NewDeregisterLogic(r.Context(), svcCtx)
		resp, err := l.Deregister()
		if err != nil {
			httpx.Error(w, err)
		} else {
			httpx.OkJson(w, resp)
		}
	}
}
