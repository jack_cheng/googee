package logic

import (
	"context"
	"database/sql"

	"googee/service/user/rpc/internal/svc"
	"googee/service/user/rpc/types/user"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type DeregisterLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewDeregisterLogic(ctx context.Context, svcCtx *svc.ServiceContext) *DeregisterLogic {
	return &DeregisterLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// 注销账号
func (l *DeregisterLogic) Deregister(in *user.DeregisterRequest) (*user.DeregisterResponse, error) {
	userInfo, err := l.svcCtx.UserModel.FindOne(l.ctx, in.Id)

	if err != nil {
		return nil, status.Error(codes.NotFound, "id was not found")
	}

	userInfo.Phone = sql.NullString{Valid: false}
	userInfo.Nickname = sql.NullString{String: "账号注销", Valid: true}
	userInfo.Uid = sql.NullString{Valid: false}
	userInfo.Invcode = sql.NullString{Valid: false}
	userInfo.Wxunionid = sql.NullString{Valid: false}
	userInfo.Wxopenid = sql.NullString{Valid: false}
	userInfo.Isderegister = 1
	l.svcCtx.UserModel.Update(l.ctx, userInfo)
	return &user.DeregisterResponse{Id: in.Id}, nil
}
