package logic

import (
	"context"
	"database/sql"

	"googee/common/errorx"
	"googee/service/user/rpc/internal/svc"
	"googee/service/user/rpc/types/user"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
)

type GetProfileLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetProfileLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetProfileLogic {
	return &GetProfileLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

//  获得用户Profile
func (l *GetProfileLogic) GetProfile(in *user.IdRequest) (*user.ProfileResponse, error) {
	profile, err := l.svcCtx.ProfileModel.FindOneByUserId(l.ctx, sql.NullInt64{Int64: in.Id, Valid: true})
	if err != nil {
		return nil, errorx.NewCodeError(10001, "用户不存在")
	}

	var resp user.ProfileResponse
	_ = copier.Copy(&resp, profile)
	return &resp, nil
}
