package logic

import (
	"context"

	"googee/service/user/rpc/internal/svc"
	"googee/service/user/rpc/types/user"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type GetUserLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetUserLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetUserLogic {
	return &GetUserLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// 获得用户信息
func (l *GetUserLogic) GetUser(in *user.IdRequest) (*user.UserResponse, error) {
	userInfo, err := l.svcCtx.UserModel.FindOne(l.ctx, in.Id)

	if err != nil {
		return nil, status.Error(codes.NotFound, "id was not found")
	}

	var userResp user.UserResponse
	_ = copier.Copy(&userResp, userInfo)
	userResp.CreateTime = userInfo.CreateTime.Unix()
	userResp.UpdateTime = userInfo.UpdateTime.Unix()
	return &userResp, nil
}
