package logic

import (
	"context"
	"database/sql"

	"googee/service/user/rpc/internal/svc"
	"googee/service/user/rpc/types/user"

	"github.com/jinzhu/copier"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"github.com/zeromicro/go-zero/core/logx"
)

type GetUserByNameLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetUserByNameLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetUserByNameLogic {
	return &GetUserByNameLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

//  根据账号获得用户信息
func (l *GetUserByNameLogic) GetUserByName(in *user.NameRequest) (*user.UserResponse, error) {
	userInfo, err := l.svcCtx.UserModel.FindOneByName(l.ctx, sql.NullString{String: in.Name, Valid: true})

	if err != nil {
		return nil, status.Error(codes.NotFound, "id was not found")
	}

	var userResp user.UserResponse
	_ = copier.Copy(&userResp, userInfo)
	userResp.CreateTime = userInfo.CreateTime.Unix()
	userResp.UpdateTime = userInfo.UpdateTime.Unix()
	return &userResp, nil
}
