package svc

import (
	"googee/service/user/model"
	"googee/service/user/rpc/internal/config"

	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

type ServiceContext struct {
	Config       config.Config
	UserModel    model.UserModel
	ProfileModel model.ProfileModel
}

func NewServiceContext(c config.Config) *ServiceContext {
	conn := sqlx.NewMysql(c.Mysql.DataSource)
	return &ServiceContext{
		Config:       c,
		UserModel:    model.NewUserModel(conn, c.CacheRedis),
		ProfileModel: model.NewProfileModel(conn, c.CacheRedis),
	}
}
