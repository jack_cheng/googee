CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(20) DEFAULT NULL COMMENT '电话',
  `gender` char(1) DEFAULT NULL COMMENT '性别 F,M,N',
  `nickname` varchar(30) DEFAULT NULL COMMENT '昵称',
  `avatar` varchar(255) DEFAULT NULL COMMENT '头像',
  `password` varchar(8) DEFAULT NULL COMMENT '密码',
  `isderegister` TINYINT(1) DEFAULT '0' COMMENT '是否注销',
  `uid` varchar(32) DEFAULT NULL COMMENT '用户ID',
  `invcode` varchar(32) DEFAULT NULL COMMENT '邀请码',
  `wxunionid` varchar(64) DEFAULT NULL COMMENT '微信unionid',
  `wxopenid` varchar(64) DEFAULT NULL COMMENT '微信Openid',
  `name` varchar(64) DEFAULT NULL COMMENT '用户名',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phone` (`phone`),
  UNIQUE KEY `name` (`name`),
  UNIQUE KEY `uid` (`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE `profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '关联user表的ID',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `wx_id` varchar(255) DEFAULT NULL COMMENT '微信号',
  `sign` varchar(255) DEFAULT NULL COMMENT '个性签名',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;