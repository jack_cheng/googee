package logic

import (
	"context"

	"googee/service/inbox/rpc/internal/svc"
	"googee/service/inbox/rpc/types/inbox"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type GetMessageLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetMessageLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetMessageLogic {
	return &GetMessageLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// 获得收件箱一条信息的内容
func (l *GetMessageLogic) GetMessage(in *inbox.IdRequest) (*inbox.InboxMessage, error) {
	resp := &inbox.InboxMessage{}

	msg, err := l.svcCtx.InboxMessageModel.FindOne(l.ctx, in.Id)
	if err != nil {
		return nil, status.Error(codes.NotFound, "msg not found")
	}

	copier.Copy(&resp, msg)

	return resp, nil
}
