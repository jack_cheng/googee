package logic

import (
	"context"

	"googee/service/inbox/rpc/internal/svc"
	"googee/service/inbox/rpc/types/inbox"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type OpenMessageLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewOpenMessageLogic(ctx context.Context, svcCtx *svc.ServiceContext) *OpenMessageLogic {
	return &OpenMessageLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// 打开阅读一个消息
func (l *OpenMessageLogic) OpenMessage(in *inbox.IdRequest) (*inbox.InboxMessage, error) {
	resp := &inbox.InboxMessage{}

	msg, err := l.svcCtx.InboxMessageModel.FindOne(l.ctx, in.Id)
	if err != nil {
		return nil, status.Error(codes.NotFound, "msg not found")
	}

	if msg.Receiver == in.UserId {
		msg.Status = 1
		l.svcCtx.InboxMessageModel.Update(l.ctx, msg)
	}

	copier.Copy(resp, msg)
	return resp, nil
}
