package logic

import (
	"context"

	"googee/service/inbox/rpc/internal/svc"
	"googee/service/inbox/rpc/types/inbox"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type GetListLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGetListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GetListLogic {
	return &GetListLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// 获得收件箱列表
func (l *GetListLogic) GetList(in *inbox.UserIdRequest) (*inbox.MessageListResponse, error) {
	resp := &inbox.MessageListResponse{}

	messages, err := l.svcCtx.InboxMessageModel.MessagePagation(l.ctx, in.UserId, in.Offset, in.Limit, in.Asc, true)
	if err != nil {
		return nil, status.Error(codes.Internal, "system error")
	}

	copier.Copy(&resp.Messagelist, messages)
	return resp, nil
}
