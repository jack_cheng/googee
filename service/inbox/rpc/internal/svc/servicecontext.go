package svc

import (
	"googee/service/inbox/model"
	"googee/service/inbox/rpc/internal/config"
	"googee/service/user/rpc/user"

	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

type ServiceContext struct {
	Config            config.Config
	InboxMessageModel model.InboxMessageModel
	SqlConn           sqlx.SqlConn
	User              user.User
}

func NewServiceContext(c config.Config) *ServiceContext {
	conn := sqlx.NewMysql(c.Mysql.DataSource)
	return &ServiceContext{
		Config:            c,
		SqlConn:           conn,
		InboxMessageModel: model.NewInboxMessageModel(conn, c.CacheRedis),
	}
}
