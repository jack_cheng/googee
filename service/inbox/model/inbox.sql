CREATE TABLE `inbox_message` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `sender` int NOT NULL DEFAULT '0' COMMENT '发送人：0表示为系统邮件',
  `receiver` int NOT NULL DEFAULT '0' COMMENT '接收人ID',
  `status` tinyint NOT NULL DEFAULT '0',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `content` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `extra` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '附件数据',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`,`sender`,`receiver`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;