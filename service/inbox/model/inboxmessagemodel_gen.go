// Code generated by goctl. DO NOT EDIT!

package model

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
	"time"

	"github.com/zeromicro/go-zero/core/stores/builder"
	"github.com/zeromicro/go-zero/core/stores/cache"
	"github.com/zeromicro/go-zero/core/stores/sqlc"
	"github.com/zeromicro/go-zero/core/stores/sqlx"
	"github.com/zeromicro/go-zero/core/stringx"
)

var (
	inboxMessageFieldNames          = builder.RawFieldNames(&InboxMessage{})
	inboxMessageRows                = strings.Join(inboxMessageFieldNames, ",")
	inboxMessageRowsExpectAutoSet   = strings.Join(stringx.Remove(inboxMessageFieldNames, "`id`", "`create_time`", "`update_time`", "`create_at`", "`update_at`"), ",")
	inboxMessageRowsWithPlaceHolder = strings.Join(stringx.Remove(inboxMessageFieldNames, "`id`", "`create_time`", "`update_time`", "`create_at`", "`update_at`"), "=?,") + "=?"

	cacheInboxMessageIdPrefix               = "cache:inboxMessage:id:"
	cacheInboxMessageIdSenderReceiverPrefix = "cache:inboxMessage:id:sender:receiver:"
)

type (
	inboxMessageModel interface {
		Insert(ctx context.Context, data *InboxMessage) (sql.Result, error)
		FindOne(ctx context.Context, id int64) (*InboxMessage, error)
		FindOneByIdSenderReceiver(ctx context.Context, id int64, sender int64, receiver int64) (*InboxMessage, error)
		Update(ctx context.Context, newData *InboxMessage) error
		Delete(ctx context.Context, id int64) error
	}

	defaultInboxMessageModel struct {
		sqlc.CachedConn
		table string
	}

	InboxMessage struct {
		Id         int64     `db:"id"`       // 主键
		Sender     int64     `db:"sender"`   // 发送人：0表示为系统邮件
		Receiver   int64     `db:"receiver"` // 接收人ID
		Status     int64     `db:"status"`
		CreateTime time.Time `db:"create_time"`
		UpdateTime time.Time `db:"update_time"`
		Content    string    `db:"content"`
		Extra      string    `db:"extra"` // 附件数据
	}
)

func newInboxMessageModel(conn sqlx.SqlConn, c cache.CacheConf) *defaultInboxMessageModel {
	return &defaultInboxMessageModel{
		CachedConn: sqlc.NewConn(conn, c),
		table:      "`inbox_message`",
	}
}

func (m *defaultInboxMessageModel) Delete(ctx context.Context, id int64) error {
	data, err := m.FindOne(ctx, id)
	if err != nil {
		return err
	}

	inboxMessageIdKey := fmt.Sprintf("%s%v", cacheInboxMessageIdPrefix, id)
	inboxMessageIdSenderReceiverKey := fmt.Sprintf("%s%v:%v:%v", cacheInboxMessageIdSenderReceiverPrefix, data.Id, data.Sender, data.Receiver)
	_, err = m.ExecCtx(ctx, func(ctx context.Context, conn sqlx.SqlConn) (result sql.Result, err error) {
		query := fmt.Sprintf("delete from %s where `id` = ?", m.table)
		return conn.ExecCtx(ctx, query, id)
	}, inboxMessageIdKey, inboxMessageIdSenderReceiverKey)
	return err
}

func (m *defaultInboxMessageModel) FindOne(ctx context.Context, id int64) (*InboxMessage, error) {
	inboxMessageIdKey := fmt.Sprintf("%s%v", cacheInboxMessageIdPrefix, id)
	var resp InboxMessage
	err := m.QueryRowCtx(ctx, &resp, inboxMessageIdKey, func(ctx context.Context, conn sqlx.SqlConn, v interface{}) error {
		query := fmt.Sprintf("select %s from %s where `id` = ? limit 1", inboxMessageRows, m.table)
		return conn.QueryRowCtx(ctx, v, query, id)
	})
	switch err {
	case nil:
		return &resp, nil
	case sqlc.ErrNotFound:
		return nil, ErrNotFound
	default:
		return nil, err
	}
}

func (m *defaultInboxMessageModel) FindOneByIdSenderReceiver(ctx context.Context, id int64, sender int64, receiver int64) (*InboxMessage, error) {
	inboxMessageIdSenderReceiverKey := fmt.Sprintf("%s%v:%v:%v", cacheInboxMessageIdSenderReceiverPrefix, id, sender, receiver)
	var resp InboxMessage
	err := m.QueryRowIndexCtx(ctx, &resp, inboxMessageIdSenderReceiverKey, m.formatPrimary, func(ctx context.Context, conn sqlx.SqlConn, v interface{}) (i interface{}, e error) {
		query := fmt.Sprintf("select %s from %s where `id` = ? and `sender` = ? and `receiver` = ? limit 1", inboxMessageRows, m.table)
		if err := conn.QueryRowCtx(ctx, &resp, query, id, sender, receiver); err != nil {
			return nil, err
		}
		return resp.Id, nil
	}, m.queryPrimary)
	switch err {
	case nil:
		return &resp, nil
	case sqlc.ErrNotFound:
		return nil, ErrNotFound
	default:
		return nil, err
	}
}

func (m *defaultInboxMessageModel) Insert(ctx context.Context, data *InboxMessage) (sql.Result, error) {
	inboxMessageIdKey := fmt.Sprintf("%s%v", cacheInboxMessageIdPrefix, data.Id)
	inboxMessageIdSenderReceiverKey := fmt.Sprintf("%s%v:%v:%v", cacheInboxMessageIdSenderReceiverPrefix, data.Id, data.Sender, data.Receiver)
	ret, err := m.ExecCtx(ctx, func(ctx context.Context, conn sqlx.SqlConn) (result sql.Result, err error) {
		query := fmt.Sprintf("insert into %s (%s) values (?, ?, ?, ?, ?)", m.table, inboxMessageRowsExpectAutoSet)
		return conn.ExecCtx(ctx, query, data.Sender, data.Receiver, data.Status, data.Content, data.Extra)
	}, inboxMessageIdKey, inboxMessageIdSenderReceiverKey)
	return ret, err
}

func (m *defaultInboxMessageModel) Update(ctx context.Context, newData *InboxMessage) error {
	data, err := m.FindOne(ctx, newData.Id)
	if err != nil {
		return err
	}

	inboxMessageIdKey := fmt.Sprintf("%s%v", cacheInboxMessageIdPrefix, data.Id)
	inboxMessageIdSenderReceiverKey := fmt.Sprintf("%s%v:%v:%v", cacheInboxMessageIdSenderReceiverPrefix, data.Id, data.Sender, data.Receiver)
	_, err = m.ExecCtx(ctx, func(ctx context.Context, conn sqlx.SqlConn) (result sql.Result, err error) {
		query := fmt.Sprintf("update %s set %s where `id` = ?", m.table, inboxMessageRowsWithPlaceHolder)
		return conn.ExecCtx(ctx, query, newData.Sender, newData.Receiver, newData.Status, newData.Content, newData.Extra, newData.Id)
	}, inboxMessageIdKey, inboxMessageIdSenderReceiverKey)
	return err
}

func (m *defaultInboxMessageModel) formatPrimary(primary interface{}) string {
	return fmt.Sprintf("%s%v", cacheInboxMessageIdPrefix, primary)
}

func (m *defaultInboxMessageModel) queryPrimary(ctx context.Context, conn sqlx.SqlConn, v, primary interface{}) error {
	query := fmt.Sprintf("select %s from %s where `id` = ? limit 1", inboxMessageRows, m.table)
	return conn.QueryRowCtx(ctx, v, query, primary)
}

func (m *defaultInboxMessageModel) tableName() string {
	return m.table
}
