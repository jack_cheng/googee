package logic

import (
	"context"
	"encoding/json"

	"googee/common/errorx"
	"googee/service/inbox/api/internal/svc"
	"googee/service/inbox/api/internal/types"
	"googee/service/inbox/rpc/types/inbox"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SendmessageLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewSendmessageLogic(ctx context.Context, svcCtx *svc.ServiceContext) *SendmessageLogic {
	return &SendmessageLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *SendmessageLogic) Sendmessage(req *types.SendMessageReq) (resp *types.SendMessageReply, err error) {
	resp = &types.SendMessageReply{}
	id, err := l.ctx.Value("userId").(json.Number).Int64()
	if err != nil {
		return nil, err
	}

	msg, err := l.svcCtx.Inbox.SendMessage(l.ctx, &inbox.SendMessageRequest{
		Sender:   id,
		Receiver: int64(req.Receiver),
		Content:  req.Content,
	})

	if err != nil {
		st, ok := status.FromError(err)
		if ok {
			if st.Code() == codes.InvalidArgument {
				return nil, errorx.NewCodeErrorNoMsg(errorx.INBOX_ERROR + 3)
			} else if st.Code() == errorx.UserNotExist {
				return nil, errorx.NewCodeErrorNoMsg(errorx.INBOX_ERROR + 2)
			} else {
				return nil, errorx.NewCodeError(errorx.INTERNAL_ERROR, "系统内部未知错误")
			}
		} else {
			return nil, errorx.NewCodeError(errorx.INTERNAL_ERROR, "系统内部未知错误")
		}
	}

	copier.Copy(&resp.Data, msg)

	return
}
