package svc

import (
	"googee/service/inbox/api/internal/config"
	"googee/service/inbox/rpc/inbox"
	"googee/service/user/rpc/user"

	"github.com/zeromicro/go-zero/zrpc"
)

type ServiceContext struct {
	Config config.Config
	Inbox  inbox.Inbox
	User   user.User
}

func NewServiceContext(c config.Config) *ServiceContext {
	return &ServiceContext{
		Config: c,
		Inbox:  inbox.NewInbox(zrpc.MustNewClient(c.Inbox)),
		User:   user.NewUser(zrpc.MustNewClient(c.User)),
	}
}
