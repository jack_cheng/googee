package logic

import (
	"fmt"
	"googee/service/mqueue/job/jobtype"

	"github.com/hibiken/asynq"
	"github.com/zeromicro/go-zero/core/logx"
)

// scheduler job ------> go-zero-looklook/app/mqueue/cmd/job/internal/logic/settleRecord.go.
func (l *MqueueScheduler) leaderboardClearScheduler() {

	task := asynq.NewTask(jobtype.ScheduleLeaderBoardClear, nil)
	// every one minute exec
	entryID, err := l.svcCtx.Scheduler.Register("*/1 * * * *", task)
	if err != nil {
		logx.WithContext(l.ctx).Errorf("!!!MqueueSchedulerErr!!! ====> leaderboardClearScheduler registered  err:%+v , task:%+v", err, task)
	}
	fmt.Printf("leaderboardClearScheduler registered an  entry: %q \n", entryID)
}
