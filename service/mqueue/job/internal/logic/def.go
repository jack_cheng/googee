package logic

type buckMinAndMaxScore struct {
	MaxScore int64 `db:"max_score"`
	MinScore int64 `db:"min_score"`
}

type buckCount struct {
	Count int64 `db:"size"`
}

const CHUNK_BLOCK int64 = 10000

// const CHUNK_BLOCK int64 = 500
const DEFAULT_SCORE_CHUNK int64 = 100
