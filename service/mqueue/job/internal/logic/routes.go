package logic

import (
	"context"
	"googee/service/mqueue/job/internal/svc"
	"googee/service/mqueue/job/jobtype"

	"github.com/hibiken/asynq"
)

type CronJob struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewCronJob(ctx context.Context, svcCtx *svc.ServiceContext) *CronJob {
	return &CronJob{
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

// register job
func (l *CronJob) Register() *asynq.ServeMux {

	mux := asynq.NewServeMux()

	//scheduler job
	mux.Handle(jobtype.ScheduleLeaderBoardSort, NewLeaderBoardSortHandler(l.svcCtx))
	mux.Handle(jobtype.ScheduleLeaderBoardClear, NewLeaderBoardClearHandler(l.svcCtx))

	//defer job
	// mux.Handle(jobtype.DeferCloseHomestayOrder,NewCloseHomestayOrderHandler(l.svcCtx))

	//queue job , asynq support queue job
	// wait you fill..

	return mux
}
