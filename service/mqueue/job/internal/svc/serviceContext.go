package svc

import (
	"googee/service/leaderboard/model"
	"googee/service/mqueue/job/internal/config"

	"github.com/hibiken/asynq"
	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

type ServiceContext struct {
	Config                   config.Config
	AsynqServer              *asynq.Server
	LeaderboardsModel        model.LeaderboardsModel
	LeaderboardsBucketsModel model.LeaderboardsBucketsModel
	LeaderboardsDataModel    model.LeaderboardsDataModel
	SqlConn                  sqlx.SqlConn
}

func NewServiceContext(c config.Config) *ServiceContext {
	conn := sqlx.NewMysql(c.Mysql.DataSource)
	return &ServiceContext{
		Config:                   c,
		AsynqServer:              newAsynqServer(c),
		LeaderboardsModel:        model.NewLeaderboardsModel(conn, c.CacheRedis),
		LeaderboardsBucketsModel: model.NewLeaderboardsBucketsModel(conn, c.CacheRedis),
		LeaderboardsDataModel:    model.NewLeaderboardsDataModel(conn, c.CacheRedis),
		SqlConn:                  conn,
	}
}
