package jobtype

// ScheduleLeaderBoardSort 排行榜数据分桶规划
const ScheduleLeaderBoardSort = "schedule:leaderboard:sort"

// ScheduleLeaderBoardClear 排行榜数据清空
const ScheduleLeaderBoardClear = "schedule:leaderboard:clear"

// DeferCloseHomestayOrder 延迟关闭订单
const DeferCloseHomestayOrder = "defer:homestay_order:close"

// MsgPaySuccessNotifyUser 支付成功通知用户
const MsgPaySuccessNotifyUser = "msg:pay_success:notify_user"
