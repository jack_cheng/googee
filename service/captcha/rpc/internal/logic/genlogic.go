package logic

import (
	"context"

	"googee/common/tool"
	"googee/service/captcha/rpc/internal/svc"
	"googee/service/captcha/rpc/types/captcha"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type GenLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewGenLogic(ctx context.Context, svcCtx *svc.ServiceContext) *GenLogic {
	return &GenLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// 获得用户信息
func (l *GenLogic) Gen(in *captcha.GenRequest) (*captcha.GenResponse, error) {
	k := in.GetKey()
	if k == "" {
		return nil, status.Error(codes.InvalidArgument, "key was not found")
	}

	_, bs4, _, _ := tool.Generate(k, l.svcCtx.Store)

	return &captcha.GenResponse{ImgBase64: bs4}, nil
}
