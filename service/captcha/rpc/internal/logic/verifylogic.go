package logic

import (
	"context"

	"googee/common/tool"
	"googee/service/captcha/rpc/internal/svc"
	"googee/service/captcha/rpc/types/captcha"

	"github.com/zeromicro/go-zero/core/logx"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type VerifyLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewVerifyLogic(ctx context.Context, svcCtx *svc.ServiceContext) *VerifyLogic {
	return &VerifyLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

// 验证验证码
func (l *VerifyLogic) Verify(in *captcha.VerifyRequest) (*captcha.VerifyResponse, error) {
	k := in.GetKey()
	if k == "" {
		return nil, status.Error(codes.InvalidArgument, "key was not found")
	}

	v := in.GetValue()
	if v == "" {
		return nil, status.Error(codes.InvalidArgument, "value was not found")
	}

	b := tool.Verify(k, v, l.svcCtx.Store)

	return &captcha.VerifyResponse{Pass: b}, nil
}
