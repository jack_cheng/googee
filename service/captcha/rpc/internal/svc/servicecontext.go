package svc

import (
	"googee/common/tool"
	"googee/service/captcha/rpc/internal/config"

	"github.com/go-redis/redis/v8"
)

type ServiceContext struct {
	Config config.Config
	Redis  *redis.Client
	Store  *tool.RedisStore
}

func NewServiceContext(c config.Config) *ServiceContext {

	client := redis.NewClient(&redis.Options{
		Addr:     c.Redis.Host,
		Password: c.Redis.Pass,
		DB:       0,   // use default DB
		PoolSize: 100, // 连接池大小
	})

	store := new(tool.RedisStore)
	store.Redis = client

	return &ServiceContext{
		Config: c,
		Redis:  client,
		Store:  store,
	}
}
