package member

import (
	"net/http"

	"github.com/zeromicro/go-zero/rest/httpx"

	"github.com/go-playground/validator"
	"googee/common/errorx"

	"googee/service/admin/api/internal/logic/member/member"
	"googee/service/admin/api/internal/svc"
	"googee/service/admin/api/internal/types"
)

func MemberListHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.ListMemberReq
		if err := httpx.Parse(r, &req); err != nil {
			httpx.Error(w, errorx.NewCodeError(3, err.Error()))
			return
		}

		if err := validator.New().StructCtx(r.Context(), req); err != nil {
			httpx.Error(w, errorx.NewCodeError(3, err.Error()))
			return
		}

		l := member.NewMemberListLogic(r.Context(), svcCtx)
		resp, err := l.MemberList(&req)
		if err != nil {
			httpx.Error(w, err)
		} else {
			httpx.OkJson(w, resp)
		}
	}
}
