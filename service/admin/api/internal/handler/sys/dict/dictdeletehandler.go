package dict

import (
	"net/http"

	"github.com/zeromicro/go-zero/rest/httpx"

	"github.com/go-playground/validator"
	"googee/common/errorx"

	"googee/service/admin/api/internal/logic/sys/dict"
	"googee/service/admin/api/internal/svc"
	"googee/service/admin/api/internal/types"
)

func DictDeleteHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.DeleteDictReq
		if err := httpx.Parse(r, &req); err != nil {
			httpx.Error(w, errorx.NewCodeError(3, err.Error()))
			return
		}

		if err := validator.New().StructCtx(r.Context(), req); err != nil {
			httpx.Error(w, errorx.NewCodeError(3, err.Error()))
			return
		}

		l := dict.NewDictDeleteLogic(r.Context(), svcCtx)
		resp, err := l.DictDelete(&req)
		if err != nil {
			httpx.Error(w, err)
		} else {
			httpx.OkJson(w, resp)
		}
	}
}
