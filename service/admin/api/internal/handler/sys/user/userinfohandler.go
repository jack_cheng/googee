package user

import (
	"net/http"

	"github.com/zeromicro/go-zero/rest/httpx"

	"googee/service/admin/api/internal/logic/sys/user"
	"googee/service/admin/api/internal/svc"
)

func UserInfoHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		l := user.NewUserInfoLogic(r.Context(), svcCtx)
		resp, err := l.UserInfo()
		if err != nil {
			httpx.Error(w, err)
		} else {
			httpx.OkJson(w, resp)
		}
	}
}
