package user

import (
	"net/http"

	"github.com/zeromicro/go-zero/rest/httpx"

	"googee/common/errorx"

	"github.com/go-playground/validator"

	"googee/service/admin/api/internal/logic/sys/user"
	"googee/service/admin/api/internal/svc"
	"googee/service/admin/api/internal/types"
)

func LogoutHandler(svcCtx *svc.ServiceContext) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req types.LogoutReq
		if err := httpx.Parse(r, &req); err != nil {
			httpx.Error(w, errorx.NewCodeError(3, err.Error()))
			return
		}

		if err := validator.New().StructCtx(r.Context(), req); err != nil {
			httpx.Error(w, errorx.NewCodeError(3, err.Error()))
			return
		}

		l := user.NewLogoutLogic(r.Context(), svcCtx)
		resp, err := l.Logout(&req, r.RemoteAddr)
		if err != nil {
			httpx.Error(w, err)
		} else {
			httpx.OkJson(w, resp)
		}
	}
}
