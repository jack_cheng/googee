package config

import (
	"github.com/zeromicro/go-zero/core/stores/cache"
	"github.com/zeromicro/go-zero/rest"
	"github.com/zeromicro/go-zero/zrpc"
)

type Config struct {
	rest.RestConf
	//系统
	Sys  zrpc.RpcClientConf
	Auth struct {
		AccessSecret string
		AccessExpire int64
	}
	Redis struct {
		Address string
		Pass    string
	}

	Mysql struct {
		DataSource string
	}

	CacheRedis cache.CacheConf
	User       zrpc.RpcClientConf
	Account    zrpc.RpcClientConf
	DtmServer  string
}
