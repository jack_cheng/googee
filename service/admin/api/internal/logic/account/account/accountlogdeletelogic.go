package account

import (
	"context"
	"strconv"

	"googee/service/admin/api/internal/svc"
	"googee/service/admin/api/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type AccountLogDeleteLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewAccountLogDeleteLogic(ctx context.Context, svcCtx *svc.ServiceContext) *AccountLogDeleteLogic {
	return &AccountLogDeleteLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *AccountLogDeleteLogic) AccountLogDelete(req *types.AccountLogDeleteReq) (*types.AccountLogDeleteResp, error) {
	id, err := strconv.ParseInt(req.Id, 10, 64)
	if err != nil {
		return &types.AccountLogDeleteResp{
			Code:    "500000",
			Message: "删除记录失败",
		}, nil
	}

	err = l.svcCtx.AccountLogModel.Delete(l.ctx, id)
	if err != nil {
		return &types.AccountLogDeleteResp{
			Code:    "500000",
			Message: "删除记录失败",
		}, nil
	}
	return &types.AccountLogDeleteResp{
		Code:    "000000",
		Message: "删除记录成功",
	}, nil
}
