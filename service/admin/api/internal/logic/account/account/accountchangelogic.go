package account

import (
	"context"

	"googee/service/account/rpc/types/account"
	"googee/service/admin/api/internal/svc"
	"googee/service/admin/api/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type AccountChangeLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewAccountChangeLogic(ctx context.Context, svcCtx *svc.ServiceContext) *AccountChangeLogic {
	return &AccountChangeLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *AccountChangeLogic) AccountChange(req *types.ChangeAccountReq) (*types.ChangeAccountResp, error) {
	l.svcCtx.Account.AddAccount(l.ctx, &account.IdAddRequest{
		Id:      req.Id,
		Field:   req.Type,
		Amount:  req.Count,
		Comment: "后台补偿",
		Typ:     "admin",
		Subtyp:  "account",
	})

	return &types.ChangeAccountResp{
		Code:    "000000",
		Message: "修改账户成功",
	}, nil

}
