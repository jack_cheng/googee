package leaderboard

import (
	"context"

	"googee/service/admin/api/internal/svc"
	"googee/service/admin/api/internal/types"
	"googee/service/leaderboard/model"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
)

type LeaderBoardUpdateLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewLeaderBoardUpdateLogic(ctx context.Context, svcCtx *svc.ServiceContext) *LeaderBoardUpdateLogic {
	return &LeaderBoardUpdateLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *LeaderBoardUpdateLogic) LeaderBoardUpdate(req *types.UpdateLeaderBoardReq) (*types.UpdateLeaderBoardResp, error) {
	m := &model.Leaderboards{}
	copier.Copy(&m, req)
	l.svcCtx.LeaderboardModel.Update(l.ctx, m)

	return &types.UpdateLeaderBoardResp{
		Code:    "000000",
		Message: "创建更新排行榜成功",
	}, nil
}
