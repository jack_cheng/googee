package leaderboard

import (
	"context"

	"googee/service/admin/api/internal/svc"
	"googee/service/admin/api/internal/types"
	"googee/service/leaderboard/model"

	"github.com/jinzhu/copier"
	"github.com/zeromicro/go-zero/core/logx"
)

type LeaderBoardAddLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewLeaderBoardAddLogic(ctx context.Context, svcCtx *svc.ServiceContext) *LeaderBoardAddLogic {
	return &LeaderBoardAddLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *LeaderBoardAddLogic) LeaderBoardAdd(req *types.AddLeaderBoardReq) (*types.AddLeaderBoardResp, error) {
	m := &model.Leaderboards{}
	copier.Copy(&m, req)
	l.svcCtx.LeaderboardModel.Insert(l.ctx, m)

	return &types.AddLeaderBoardResp{
		Code:    "000000",
		Message: "创建创建排行榜成功",
	}, nil
}
