package leaderboard

import (
	"context"

	"googee/service/admin/api/internal/svc"
	"googee/service/admin/api/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type LeaderBoardDeleteLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewLeaderBoardDeleteLogic(ctx context.Context, svcCtx *svc.ServiceContext) *LeaderBoardDeleteLogic {
	return &LeaderBoardDeleteLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *LeaderBoardDeleteLogic) LeaderBoardDelete(req *types.DeleteLeaderBoardReq) (*types.DeleteLeaderBoardResp, error) {
	l.svcCtx.LeaderboardModel.Delete(l.ctx, req.Id)

	return &types.DeleteLeaderBoardResp{
		Code:    "000000",
		Message: "注销会员信息成功",
	}, nil
}
