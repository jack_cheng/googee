package leaderboard

import (
	"context"
	"time"

	"googee/common/utils"
	"googee/service/admin/api/internal/svc"
	"googee/service/admin/api/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type LeaderBoardListLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewLeaderBoardListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *LeaderBoardListLogic {
	return &LeaderBoardListLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *LeaderBoardListLogic) LeaderBoardList(req *types.ListLeaderBoardReq) (*types.ListLeaderBoardResp, error) {
	all, err := l.svcCtx.LeaderboardModel.FindAllEx(req.Current, req.PageSize)

	if err != nil {
		return &types.ListLeaderBoardResp{
			Current:  req.Current,
			PageSize: req.PageSize,
			Success:  false,
			Code:     "500000",
			Message:  "查询排行榜列表失败",
		}, nil
	}

	count, _ := l.svcCtx.LeaderboardModel.Count()
	var list []*types.ListLeaderBoardData
	utils.MyCopier(&list, all)

	n := time.Now()
	for _, c := range list {
		if c.Typ == "Y" && c.Cron != "" {
			c.Prev = utils.PrevCronTime(c.Cron, n).Format("2006-01-02 15:04:05")
			c.Next = utils.NextCronTime(c.Cron, n).Format("2006-01-02 15:04:05")
		}
	}

	return &types.ListLeaderBoardResp{
		Current:  req.Current,
		Data:     list,
		PageSize: req.PageSize,
		Success:  true,
		Total:    count,
		Code:     "000000",
		Message:  "查询排行榜信息成功",
	}, nil
}
