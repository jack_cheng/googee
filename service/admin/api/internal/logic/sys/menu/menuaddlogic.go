package menu

import (
	"context"
	"encoding/json"

	"googee/common/errorx"
	"googee/service/admin/api/internal/svc"
	"googee/service/admin/api/internal/types"
	"googee/service/admin/rpc/sys"

	"github.com/zeromicro/go-zero/core/logx"
)

type MenuAddLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewMenuAddLogic(ctx context.Context, svcCtx *svc.ServiceContext) *MenuAddLogic {
	return &MenuAddLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *MenuAddLogic) MenuAdd(req *types.AddMenuReq) (*types.AddMenuResp, error) {
	userId, _ := l.ctx.Value("userId").(json.Number).Int64()
	//获得用户信息
	uinfo, err := l.svcCtx.Sys.UserInfo(l.ctx, &sys.InfoReq{
		UserId: userId,
	})

	if err != nil {
		return nil, errorx.NewDefaultError("添加菜单失败")
	}

	_, err = l.svcCtx.Sys.MenuAdd(l.ctx, &sys.MenuAddReq{
		Name:          req.Name,
		ParentId:      req.ParentId,
		Url:           req.Url,
		Perms:         req.Perms,
		Type:          req.Type,
		Icon:          req.Icon,
		OrderNum:      req.OrderNum,
		CreateBy:      uinfo.Name,
		VuePath:       req.VuePath,
		VueComponent:  req.VueComponent,
		VueIcon:       req.VueIcon,
		VueRedirect:   req.VueRedirect,
		BackGroundUrl: req.BackGroundUrl,
	})

	if err != nil {
		reqStr, _ := json.Marshal(req)
		logx.WithContext(l.ctx).Errorf("添加菜单信息失败,参数:%s,异常:%s", reqStr, err.Error())
		return nil, errorx.NewDefaultError("添加菜单失败")
	}

	return &types.AddMenuResp{
		Code:    "000000",
		Message: "添加菜单成功",
	}, nil
}
