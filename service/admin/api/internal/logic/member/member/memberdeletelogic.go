package member

import (
	"context"

	"googee/service/admin/api/internal/svc"
	"googee/service/admin/api/internal/types"
	"googee/service/user/rpc/user"

	"github.com/zeromicro/go-zero/core/logx"
)

type MemberDeleteLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewMemberDeleteLogic(ctx context.Context, svcCtx *svc.ServiceContext) *MemberDeleteLogic {
	return &MemberDeleteLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *MemberDeleteLogic) MemberDelete(req *types.DeleteMemberReq) (resp *types.DeleteMemberResp, err error) {
	l.svcCtx.User.Deregister(l.ctx, &user.DeregisterRequest{
		Id: req.Id,
	})
	return &types.DeleteMemberResp{
		Code:    "000000",
		Message: "注销会员信息成功",
	}, nil
}
