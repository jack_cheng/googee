package member

import (
	"context"
	"encoding/json"

	"googee/common/utils"
	"googee/service/admin/api/internal/svc"
	"googee/service/admin/api/internal/types"

	"github.com/zeromicro/go-zero/core/logx"
)

type MemberListLogic struct {
	logx.Logger
	ctx    context.Context
	svcCtx *svc.ServiceContext
}

func NewMemberListLogic(ctx context.Context, svcCtx *svc.ServiceContext) *MemberListLogic {
	return &MemberListLogic{
		Logger: logx.WithContext(ctx),
		ctx:    ctx,
		svcCtx: svcCtx,
	}
}

func (l *MemberListLogic) MemberList(req *types.ListMemberReq) (*types.ListMemberResp, error) {

	all, err := l.svcCtx.UserModel.FindAllEx(req.Current, req.PageSize)

	if err != nil {
		reqStr, _ := json.Marshal(req)
		logx.WithContext(l.ctx).Errorf("查询用户列表信息失败,参数:%s,异常:%s", reqStr, err.Error())
		return nil, err
	}

	count, _ := l.svcCtx.UserModel.Count()
	var list []*types.ListMemberData

	utils.MyCopier(&list, all)

	// copier.Copy(&list, all)
	// for i, a := range *all {
	// 	list[i].CreateTime = a.CreateTime.String()
	// }

	return &types.ListMemberResp{
		Current:  req.Current,
		Data:     list,
		PageSize: req.PageSize,
		Success:  true,
		Total:    count,
		Code:     "000000",
		Message:  "查询会员信息成功",
	}, nil
}
