package logic

import (
	"context"
	"time"

	"googee/service/admin/rpc/internal/svc"
	"googee/service/admin/rpc/types/sys"

	"github.com/zeromicro/go-zero/core/logx"
)

type MenuUpdateLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewMenuUpdateLogic(ctx context.Context, svcCtx *svc.ServiceContext) *MenuUpdateLogic {
	return &MenuUpdateLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *MenuUpdateLogic) MenuUpdate(in *sys.MenuUpdateReq) (*sys.MenuUpdateResp, error) {
	menu, err := l.svcCtx.SysMenuModel.FindOne(l.ctx, in.Id)

	if err != nil {
		return nil, err
	}

	menu.Name = in.Name
	menu.ParentId = in.ParentId
	menu.Url = in.Url
	menu.Perms = in.Perms
	menu.Type = in.Type
	menu.Icon = in.Icon
	menu.OrderNum = in.OrderNum
	menu.LastUpdateBy = in.LastUpdateBy
	menu.LastUpdateTime = time.Now()
	menu.DelFlag = 0
	menu.VuePath = in.VuePath
	menu.VueIcon = in.VueIcon
	menu.VueComponent = in.VueComponent
	menu.VueIcon = in.VueIcon
	menu.VueRedirect = in.VueRedirect
	menu.BackgroundUrl = in.BackGroundUrl

	err = l.svcCtx.SysMenuModel.Update(l.ctx, menu)

	if err != nil {
		return nil, err
	}

	return &sys.MenuUpdateResp{}, nil
}
