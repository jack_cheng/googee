package logic

import (
	"context"

	"googee/service/admin/rpc/internal/svc"
	"googee/service/admin/rpc/types/sys"

	"github.com/zeromicro/go-zero/core/logx"
)

type SysLogDeleteLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewSysLogDeleteLogic(ctx context.Context, svcCtx *svc.ServiceContext) *SysLogDeleteLogic {
	return &SysLogDeleteLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *SysLogDeleteLogic) SysLogDelete(in *sys.SysLogDeleteReq) (*sys.SysLogDeleteResp, error) {
	err := l.svcCtx.SysLogModel.Delete(l.ctx, in.Id)

	if err != nil {
		return nil, err
	}

	return &sys.SysLogDeleteResp{}, nil
}
