package logic

import (
	"context"
	"time"

	"googee/service/admin/model"
	"googee/service/admin/rpc/internal/svc"
	"googee/service/admin/rpc/types/sys"

	"github.com/zeromicro/go-zero/core/logx"
)

type UpdateMenuRoleLogic struct {
	ctx    context.Context
	svcCtx *svc.ServiceContext
	logx.Logger
}

func NewUpdateMenuRoleLogic(ctx context.Context, svcCtx *svc.ServiceContext) *UpdateMenuRoleLogic {
	return &UpdateMenuRoleLogic{
		ctx:    ctx,
		svcCtx: svcCtx,
		Logger: logx.WithContext(ctx),
	}
}

func (l *UpdateMenuRoleLogic) UpdateMenuRole(in *sys.UpdateMenuRoleReq) (*sys.UpdateMenuRoleResp, error) {
	_ = l.svcCtx.SysRoleMenuModel.Delete(l.ctx, in.RoleId)

	ids := in.MenuIds
	for _, id := range ids {
		_, _ = l.svcCtx.SysRoleMenuModel.Insert(l.ctx, &model.SysRoleMenu{
			RoleId:         in.RoleId,
			MenuId:         id,
			CreateBy:       "admin",
			CreateTime:     time.Now(),
			LastUpdateBy:   "admin",
			LastUpdateTime: time.Now(),
		})
	}

	return &sys.UpdateMenuRoleResp{}, nil
}
