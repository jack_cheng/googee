package svc

import (
	"googee/service/admin/model"
	"googee/service/admin/rpc/internal/config"

	"github.com/zeromicro/go-zero/core/stores/sqlx"
)

type ServiceContext struct {
	Config           config.Config
	SysUserModel     model.SysUserModel
	SysMenuModel     model.SysMenuModel
	SysRoleModel     model.SysRoleModel
	SysDictModel     model.SysDictModel
	SysDeptModel     model.SysDeptModel
	SysJobModel      model.SysJobModel
	SysLoginLogModel model.SysLoginLogModel
	SysLogModel      model.SysLogModel
	SysUserRoleModel model.SysUserRoleModel
	SysRoleMenuModel model.SysRoleMenuModel
}

func NewServiceContext(c config.Config) *ServiceContext {
	conn := sqlx.NewMysql(c.Mysql.DataSource)
	return &ServiceContext{
		Config:           c,
		SysUserModel:     model.NewSysUserModel(conn, c.CacheRedis),
		SysMenuModel:     model.NewSysMenuModel(conn, c.CacheRedis),
		SysRoleModel:     model.NewSysRoleModel(conn, c.CacheRedis),
		SysDictModel:     model.NewSysDictModel(conn, c.CacheRedis),
		SysDeptModel:     model.NewSysDeptModel(conn, c.CacheRedis),
		SysJobModel:      model.NewSysJobModel(conn, c.CacheRedis),
		SysLoginLogModel: model.NewSysLoginLogModel(conn, c.CacheRedis),
		SysLogModel:      model.NewSysLogModel(conn, c.CacheRedis),
		SysUserRoleModel: model.NewSysUserRoleModel(conn, c.CacheRedis),
		SysRoleMenuModel: model.NewSysRoleMenuModel(conn, c.CacheRedis),
	}
}
