package t

import (
	tt "googee/t"
	"net/http"
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/gavv/httpexpect/v2"
)

type createReq struct {
	AdCode    int    `json:"ad_code"`
	Phone     string `json:"phone"`
	Street    string `json:"street"`
	IsDefault int    `json:"is_default"`
}

func TestAddrBook001(t *testing.T) {
	t.Log("创建一个默认收货地址")
	tt.ClearDb()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, _ := tt.GetRegisterJWT(a.UserName, a.Password)
	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)

	e.POST("/api/addrbook/create").WithJSON(&createReq{
		AdCode:    210211,
		Phone:     "18887776543",
		Street:    "象牙山村",
		IsDefault: 1,
	}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().Object().ContainsKey("code").ValueEqual("code", 0)

	e.GET("/api/addrbook/list").WithHeader("Authorization", jwt).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().
		ContainsKey("booklist").Value("booklist").
		Array().Length().Equal(1)

}

func TestAddrBook002(t *testing.T) {
	t.Log("创建一个默认收货地址，然后删除，获得地址列表为空")
	tt.ClearDb()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, _ := tt.GetRegisterJWT(a.UserName, a.Password)
	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)

	e.POST("/api/addrbook/create").WithJSON(&createReq{
		AdCode:    210211,
		Phone:     "18887776543",
		Street:    "象牙山村",
		IsDefault: 1,
	}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().Object().ContainsKey("code").ValueEqual("code", 0)

	e.GET("/api/addrbook/del/"+"1").WithHeader("Authorization", jwt).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0)

	e.GET("/api/addrbook/list").WithHeader("Authorization", jwt).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().ContainsKey("booklist").ValueEqual("booklist", nil)

}

func TestAddrBook003(t *testing.T) {
	t.Log("创建默认地址失败，传入的参数，ad_code不正确")
	tt.ClearDb()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, _ := tt.GetRegisterJWT(a.UserName, a.Password)
	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)

	e.POST("/api/addrbook/create").WithJSON(&createReq{
		AdCode:    210211234234,
		Phone:     "18887776543",
		Street:    "象牙山村",
		IsDefault: 1,
	}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().Object().ContainsKey("code").ValueNotEqual("code", 0)

	e.GET("/api/addrbook/list").WithHeader("Authorization", jwt).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().ContainsKey("booklist").ValueEqual("booklist", nil)
}

func TestAddrBook004(t *testing.T) {
	t.Log("创建默认地址失败，传入的参数，手机号格式不正确")
	tt.ClearDb()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, _ := tt.GetRegisterJWT(a.UserName, a.Password)
	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)

	e.POST("/api/addrbook/create").WithJSON(&createReq{
		AdCode:    210211,
		Phone:     "1888777654",
		Street:    "象牙山村",
		IsDefault: 1,
	}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().Object().ContainsKey("code").ValueNotEqual("code", 0)

	e.GET("/api/addrbook/list").WithHeader("Authorization", jwt).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().ContainsKey("booklist").ValueEqual("booklist", nil)
}

func TestAddrBook005(t *testing.T) {
	t.Log("创建5个默认收货地址")
	tt.ClearDb()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, _ := tt.GetRegisterJWT(a.UserName, a.Password)
	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)

	for i := 0; i < 5; i++ {
		e.POST("/api/addrbook/create").WithJSON(&createReq{
			AdCode:    210211,
			Phone:     "18887776543",
			Street:    "象牙山村",
			IsDefault: 1,
		}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
			JSON().Object().ContainsKey("code").ValueEqual("code", 0)
	}

	e.GET("/api/addrbook/list").WithHeader("Authorization", jwt).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().
		ContainsKey("booklist").Value("booklist").
		Array().Length().Equal(5)

}

func TestAddrBook006(t *testing.T) {
	t.Log("创建一个默认收货地址，然后另外一个用户尝试删除这个地址")
	tt.ClearDb()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, _ := tt.GetRegisterJWT(a.UserName, a.Password)
	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)

	e.POST("/api/addrbook/create").WithJSON(&createReq{
		AdCode:    210211,
		Phone:     "18887776543",
		Street:    "象牙山村",
		IsDefault: 1,
	}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().Object().ContainsKey("code").ValueEqual("code", 0)

	b := SomeStructWithTags{}
	err = faker.FakeData(&b)
	if err != nil {
		panic(err)
	}

	jwt1, _ := tt.GetRegisterJWT(b.UserName, b.Password)
	e.GET("/api/addrbook/del/"+"1").WithHeader("Authorization", jwt1).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueNotEqual("code", 0)

	e.GET("/api/addrbook/list").WithHeader("Authorization", jwt).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().
		ContainsKey("booklist").Value("booklist").
		Array().Length().Equal(1)

}

func TestAddrBook007(t *testing.T) {
	t.Log("创建一个默认收货地址，然后另外一个用户尝试获取这个地址")
	tt.ClearDb()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, _ := tt.GetRegisterJWT(a.UserName, a.Password)
	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)

	e.POST("/api/addrbook/create").WithJSON(&createReq{
		AdCode:    210211,
		Phone:     "18887776543",
		Street:    "象牙山村",
		IsDefault: 1,
	}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().Object().ContainsKey("code").ValueEqual("code", 0)

	b := SomeStructWithTags{}
	err = faker.FakeData(&b)
	if err != nil {
		panic(err)
	}

	jwt1, _ := tt.GetRegisterJWT(b.UserName, b.Password)
	e.GET("/api/addrbook/"+"1").WithHeader("Authorization", jwt1).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueNotEqual("code", 0)

	e.GET("/api/addrbook/list").WithHeader("Authorization", jwt).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().
		ContainsKey("booklist").Value("booklist").
		Array().Length().Equal(1)

}
