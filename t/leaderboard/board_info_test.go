package t

import (
	tt "googee/t"
	"net/http"
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/gavv/httpexpect/v2"
)

func TestAllBoard01(t *testing.T) {
	t.Log("测试获得所有排行榜的信息")
	tt.ClearDb()

	tt.InsertBoard("每周积分排行榜", "N", "每周积分排行榜，过了一周以后就删除了")
	tt.InsertBoard("每周", "N", "每周排行榜活动")

	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, _ := tt.GetRegisterJWT(a.UserName, a.Password)
	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)
	e.GET("/api/leaderboard/boards").WithHeader("Authorization", jwt).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().
		ContainsKey("boards").Value("boards").
		Array().Length().Equal(2)
}

func TestAllBoard02(t *testing.T) {
	t.Log("测试添加2个排行榜删除一个，以后就剩下一个")
	tt.ClearDb()

	tt.InsertBoard("每周积分排行榜", "N", "每周积分排行榜，过了一周以后就删除了")
	tt.InsertBoard("每周", "N", "每周排行榜活动")

	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, _ := tt.GetRegisterJWT(a.UserName, a.Password)
	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)
	e.GET("/api/leaderboard/boards").WithHeader("Authorization", jwt).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().
		ContainsKey("boards").Value("boards").
		Array().Length().Equal(2)

	tt.DelBoard(1)
	e.GET("/api/leaderboard/boards").WithHeader("Authorization", jwt).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().
		ContainsKey("boards").Value("boards").
		Array().Length().Equal(1)

}
