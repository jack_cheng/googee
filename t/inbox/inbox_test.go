package t

import (
	tt "googee/t"
	"net/http"
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/gavv/httpexpect/v2"
)

type sendMessageReq struct {
	Receiver int64  `json:"receiver"`
	Content  string `json:"content"`
}

func TestSendMessage01(t *testing.T) {
	t.Log("发送mesage到inbox")
	tt.ClearDb()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	_, id0 := tt.GetRegisterJWT(a.UserName, a.Password)

	b := SomeStructWithTags{}
	err = faker.FakeData(&b)
	if err != nil {
		panic(err)
	}
	jwt, _ := tt.GetRegisterJWT(b.UserName, b.Password)
	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)
	e.POST("/api/inbox/sendmessage").WithJSON(&sendMessageReq{
		Content:  "hello world",
		Receiver: id0,
	}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().Object().ContainsKey("code").ValueEqual("code", 0)
}

func TestSendMessage02(t *testing.T) {
	t.Log("获得已发送的单条的消息的内容")
	tt.ClearDb()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt0, id0 := tt.GetRegisterJWT(a.UserName, a.Password)

	b := SomeStructWithTags{}
	err = faker.FakeData(&b)
	if err != nil {
		panic(err)
	}
	jwt, _ := tt.GetRegisterJWT(b.UserName, b.Password)
	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)
	e.POST("/api/inbox/sendmessage").WithJSON(&sendMessageReq{
		Content:  "hello world",
		Receiver: id0,
	}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().Object().ContainsKey("code").ValueEqual("code", 0)

	//查看一条消息
	e.GET("/api/inbox/message/1").WithHeader("Authorization", jwt0).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().
		ContainsKey("content").ValueEqual("content", "hello world").
		ContainsKey("status").ValueEqual("status", 0)

	e.GET("/api/inbox/message/1").WithHeader("Authorization", jwt).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().
		ContainsKey("content").ValueEqual("content", "hello world").
		ContainsKey("status").ValueEqual("status", 0)
}

func TestSendMessage03(t *testing.T) {
	t.Log("获得已发送的单条的消息的内容")
	tt.ClearDb()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt0, id0 := tt.GetRegisterJWT(a.UserName, a.Password)

	b := SomeStructWithTags{}
	err = faker.FakeData(&b)
	if err != nil {
		panic(err)
	}
	jwt, _ := tt.GetRegisterJWT(b.UserName, b.Password)
	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)
	e.POST("/api/inbox/sendmessage").WithJSON(&sendMessageReq{
		Content:  "hello world",
		Receiver: id0,
	}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().Object().ContainsKey("code").ValueEqual("code", 0)

	//查看一条消息
	e.GET("/api/inbox/message/1").WithHeader("Authorization", jwt0).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().
		ContainsKey("content").ValueEqual("content", "hello world").
		ContainsKey("status").ValueEqual("status", 0)

	e.GET("/api/inbox/message/1").WithHeader("Authorization", jwt).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().
		ContainsKey("content").ValueEqual("content", "hello world").
		ContainsKey("status").ValueEqual("status", 0)

	e.GET("/api/inbox/openmessage").WithQuery("id", 1).WithHeader("Authorization", jwt0).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().
		ContainsKey("content").ValueEqual("content", "hello world").
		ContainsKey("status").ValueEqual("status", 1)

	//查看一条消息
	e.GET("/api/inbox/message/1").WithHeader("Authorization", jwt0).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().
		ContainsKey("content").ValueEqual("content", "hello world").
		ContainsKey("status").ValueEqual("status", 1)

	e.GET("/api/inbox/message/1").WithHeader("Authorization", jwt).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().
		ContainsKey("content").ValueEqual("content", "hello world").
		ContainsKey("status").ValueEqual("status", 1)
}

func TestSendMessageList01(t *testing.T) {
	t.Log("发送一个消息，消息列表中，列表的个数为1，测试发送和接收人的ID正确")
	tt.ClearDb()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	_, id0 := tt.GetRegisterJWT(a.UserName, a.Password)

	b := SomeStructWithTags{}
	err = faker.FakeData(&b)
	if err != nil {
		panic(err)
	}
	jwt, _ := tt.GetRegisterJWT(b.UserName, b.Password)
	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)
	e.POST("/api/inbox/sendmessage").WithJSON(&sendMessageReq{
		Content:  "hello world",
		Receiver: id0,
	}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().Object().ContainsKey("code").ValueEqual("code", 0)

	//获得我inbox列表
	e.POST("/api/inbox/message").WithJSON(map[string]interface{}{
		"offset": 0,
		"limit":  10,
		"asc":    true,
	}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").
		Array().Length().Equal(1)

	e.POST("/api/inbox/message").WithJSON(map[string]interface{}{
		"offset": 0,
		"limit":  10,
		"asc":    true,
	}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").
		Array().Element(0).Object().
		ContainsKey("sender").ValueEqual("sender", 2).
		ContainsKey("receiver").ValueEqual("receiver", 1).
		ContainsKey("status").ValueEqual("status", 0)
}

func TestSendMessageList02(t *testing.T) {
	t.Log("发送一个消息，然后读一下，这个消息，在消息列表中，此消息状态变成0")
	tt.ClearDb()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt0, id0 := tt.GetRegisterJWT(a.UserName, a.Password)

	b := SomeStructWithTags{}
	err = faker.FakeData(&b)
	if err != nil {
		panic(err)
	}
	jwt, _ := tt.GetRegisterJWT(b.UserName, b.Password)
	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)
	e.POST("/api/inbox/sendmessage").WithJSON(&sendMessageReq{
		Content:  "hello world",
		Receiver: id0,
	}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().Object().ContainsKey("code").ValueEqual("code", 0)

	//获得我inbox列表
	e.POST("/api/inbox/message").WithJSON(map[string]interface{}{
		"offset": 0,
		"limit":  10,
		"asc":    true,
	}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").
		Array().Length().Equal(1)

	e.GET("/api/inbox/openmessage").WithQuery("id", 1).WithHeader("Authorization", jwt0).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().
		ContainsKey("content").ValueEqual("content", "hello world").
		ContainsKey("status").ValueEqual("status", 1)

	e.POST("/api/inbox/message").WithJSON(map[string]interface{}{
		"offset": 0,
		"limit":  10,
		"asc":    true,
	}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").
		Array().Element(0).Object().
		ContainsKey("sender").ValueEqual("sender", 2).
		ContainsKey("receiver").ValueEqual("receiver", 1).
		ContainsKey("status").ValueEqual("status", 1)
}

func TestSendMessageListPage01(t *testing.T) {
	t.Log("发送一个消息，测试消息分页")
	tt.ClearDb()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	_, id0 := tt.GetRegisterJWT(a.UserName, a.Password)

	b := SomeStructWithTags{}
	err = faker.FakeData(&b)
	if err != nil {
		panic(err)
	}
	jwt, _ := tt.GetRegisterJWT(b.UserName, b.Password)
	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)

	// 发送
	for i := 0; i < 10; i++ {
		e.POST("/api/inbox/sendmessage").WithJSON(&sendMessageReq{
			Content:  "hello world",
			Receiver: id0,
		}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
			JSON().Object().ContainsKey("code").ValueEqual("code", 0)
	}

	//获得我inbox列表
	e.POST("/api/inbox/message").WithJSON(map[string]interface{}{
		"offset": 0,
		"limit":  5,
		"asc":    true,
	}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").
		Array().Length().Equal(5)

	e.POST("/api/inbox/message").WithJSON(map[string]interface{}{
		"offset": 6,
		"limit":  5,
		"asc":    true,
	}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").
		Array().Length().Equal(4)

	e.POST("/api/inbox/message").WithJSON(map[string]interface{}{
		"offset": 10,
		"limit":  5,
		"asc":    true,
	}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").ValueEqual("data", nil)
}

func TestSendMessageListPage02(t *testing.T) {
	t.Log("发送一个消息，测试消息分页")
	tt.ClearDb()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	_, id0 := tt.GetRegisterJWT(a.UserName, a.Password)

	b := SomeStructWithTags{}
	err = faker.FakeData(&b)
	if err != nil {
		panic(err)
	}
	jwt, _ := tt.GetRegisterJWT(b.UserName, b.Password)
	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)

	// 发送
	for i := 0; i < 10; i++ {
		e.POST("/api/inbox/sendmessage").WithJSON(&sendMessageReq{
			Content:  "hello world",
			Receiver: id0,
		}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
			JSON().Object().ContainsKey("code").ValueEqual("code", 0)
	}

	//获得我inbox列表
	for i := 1; i <= 5; i++ {
		e.POST("/api/inbox/message").WithJSON(map[string]interface{}{
			"offset": 0,
			"limit":  5,
			"asc":    true,
		}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
			JSON().
			Object().ContainsKey("code").ValueEqual("code", 0).
			ContainsKey("data").Value("data").
			Array().
			Element(i-1).Object().ContainsKey("id").ValueEqual("id", i)
	}

	for i := 6; i <= 8; i++ {
		e.POST("/api/inbox/message").WithJSON(map[string]interface{}{
			"offset": 5,
			"limit":  3,
			"asc":    true,
		}).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
			JSON().
			Object().ContainsKey("code").ValueEqual("code", 0).
			ContainsKey("data").Value("data").
			Array().
			Element(i-6).Object().ContainsKey("id").ValueEqual("id", i)
	}

}
