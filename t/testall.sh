#/bin/bash
if [ -a test_report.html ]; then rm test_report.html; fi;
go test -v -timeout 90s --count=1 ./... -json | go-test-report && open test_report.html