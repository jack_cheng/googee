package t

import (
	tt "googee/t"
	"net/http"
	"testing"

	"github.com/gavv/httpexpect/v2"
)

func TestRegister01(t *testing.T) {
	t.Log("测试用户名不足6位的情况")
	tt.ClearDb()

	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)

	// every response should have this header
	m := e.Matcher(func(resp *httpexpect.Response) {
		// resp.Header("API-Version").NotEmpty()
	})

	post_data := map[string]interface{}{
		"username": "a1",
		"password": "1231231231",
	}

	m.POST("/api/user/register").WithJSON(post_data).Expect().Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueNotEqual("code", 0)
}

func TestRegister02(t *testing.T) {
	t.Log("测试密码不足6为的情况")
	tt.ClearDb()

	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)

	// every response should have this header
	m := e.Matcher(func(resp *httpexpect.Response) {
		// resp.Header("API-Version").NotEmpty()
	})

	post_data := map[string]interface{}{
		"username": "a1122222222",
		"password": "1234",
	}

	m.POST("/api/user/register").WithJSON(post_data).Expect().Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueNotEqual("code", 0)
}

func TestRegister03(t *testing.T) {
	t.Log("账户和密码都足够的情况")
	tt.ClearDb()

	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)

	// every response should have this header
	m := e.Matcher(func(resp *httpexpect.Response) {
		// resp.Header("API-Version").NotEmpty()
	})

	post_data := map[string]interface{}{
		"username": "a123456",
		"password": "12345678",
	}

	m.POST("/api/user/register").WithJSON(post_data).Expect().Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0)
}

func TestRegister04(t *testing.T) {
	t.Log("账号已经注册了，不能重复注册")
	tt.ClearDb()

	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)

	// every response should have this header
	m := e.Matcher(func(resp *httpexpect.Response) {
		// resp.Header("API-Version").NotEmpty()
	})

	post_data := map[string]interface{}{
		"username": "a123456",
		"password": "12345678",
	}

	m.POST("/api/user/register").WithJSON(post_data).Expect().Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0)
	m.POST("/api/user/register").WithJSON(post_data).Expect().Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueNotEqual("code", 0)
}
