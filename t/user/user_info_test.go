package t

import (
	"fmt"
	tt "googee/t"
	"net/http"
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/gavv/httpexpect/v2"
)

func TestUserInfo01(t *testing.T) {
	t.Log("注册一个用户获取自己的信息")
	tt.ClearDb()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, id := tt.GetRegisterJWT(a.UserName, a.Password)
	c := tt.GetConfig()

	e := httpexpect.New(t, c.API)
	e.GET(fmt.Sprintf("/api/user/info/%d", id)).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0)
}

func TestUserInfo02(t *testing.T) {
	t.Log("注册一个用户获得一个不存在用户的信息")
	tt.ClearDb()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, _ := tt.GetRegisterJWT(a.UserName, a.Password)

	id2 := 2
	c := tt.GetConfig()

	e := httpexpect.New(t, c.API)
	e.GET(fmt.Sprintf("/api/user/info/%d", id2)).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueNotEqual("code", 0)
}

func TestUserInfo03(t *testing.T) {
	t.Log("注册2个账号，一个账号注销，一个账号去获得注销账号的信息，依然是可以获得的")
	tt.ClearDb()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	b := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	err = faker.FakeData(&b)
	if err != nil {
		panic(err)
	}

	jwt1, id1 := tt.GetRegisterJWT(a.UserName, a.Password)
	jwt2, id2 := tt.GetRegisterJWT(b.UserName, b.Password)

	c := tt.GetConfig()

	e := httpexpect.New(t, c.API)
	e.GET(fmt.Sprintf("/api/user/info/%d", id2)).WithHeader("Authorization", jwt1).
		Expect().Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0)

	e.GET(fmt.Sprintf("/api/user/info/%d", id1)).WithHeader("Authorization", jwt2).
		Expect().Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0)

	e.GET("/api/user/deregister").WithHeader("Authorization", jwt1).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0)

	e.GET(fmt.Sprintf("/api/user/info/%d", id2)).WithHeader("Authorization", jwt1).
		Expect().Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0)

	e.GET(fmt.Sprintf("/api/user/info/%d", id1)).WithHeader("Authorization", jwt2).
		Expect().Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0)
}
