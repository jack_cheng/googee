package t

import (
	tt "googee/t"
	"net/http"
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/gavv/httpexpect/v2"
	"gopkg.in/go-playground/assert.v1"
)

func TestDeregister01(t *testing.T) {
	t.Log("注册一个账号然后去注销")
	tt.ClearDb()
	c := tt.GetConfig()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, _ := tt.GetRegisterJWT(a.UserName, a.Password)
	e := httpexpect.New(t, c.API)
	e.GET("/api/user/deregister").WithHeader("Authorization", jwt).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0)

}

func TestDeregister02(t *testing.T) {
	t.Log("创建账号注销以后无法登录")
	tt.ClearDb()
	c := tt.GetConfig()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, _ := tt.GetRegisterJWT(a.UserName, a.Password)
	e := httpexpect.New(t, c.API)
	e.GET("/api/user/deregister").WithHeader("Authorization", jwt).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0)

	jwt, _ = tt.GetLoginJWT(a.UserName, a.Password)
	assert.Equal(t, jwt, "")
}

func TestDeregister03(t *testing.T) {
	t.Log("账号注销完毕，之后，注销的账号不能再次登录，也不能注册相同的账号")
	tt.ClearDb()
	c := tt.GetConfig()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, _ := tt.GetRegisterJWT(a.UserName, a.Password)
	e := httpexpect.New(t, c.API)
	e.GET("/api/user/deregister").WithHeader("Authorization", jwt).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0)

	jwt, _ = tt.GetLoginJWT(a.UserName, a.Password)
	assert.Equal(t, jwt, "")

	jwt, _ = tt.GetRegisterJWT(a.UserName, a.Password)
	assert.Equal(t, jwt, "")
}
