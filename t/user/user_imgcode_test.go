package t

import (
	tt "googee/t"
	"net/http"
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/fatih/color"
	"github.com/gavv/httpexpect/v2"
)

func TestImgeCode01(t *testing.T) {
	t.Log("测试发送密码")
	tt.ClearDb()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, _ := tt.GetRegisterJWT(a.UserName, a.Password)
	c := tt.GetConfig()

	e := httpexpect.New(t, c.API)
	e.GET("/api/user/bindphone/imgcode/"+tt.RandomPhone()).WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0)
}

func TestImgeCode02(t *testing.T) {
	t.Log("手机号错误无法发送")
	tt.ClearDb()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, _ := tt.GetRegisterJWT(a.UserName, a.Password)
	c := tt.GetConfig()

	e := httpexpect.New(t, c.API)
	e.GET("/api/user/bindphone/imgcode/"+a.PhoneNumber).WithHeader("Authorization", jwt).
		Expect().Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueNotEqual("code", 0)
}

func TestImgeCode03(t *testing.T) {
	t.Log("下发的数据中有captcha数据")
	tt.ClearDb()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, _ := tt.GetRegisterJWT(a.UserName, a.Password)
	c := tt.GetConfig()

	e := httpexpect.New(t, c.API)
	e.GET("/api/user/bindphone/imgcode/"+tt.RandomPhone()).WithHeader("Authorization", jwt).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("data").Value("data").Object().ContainsKey("captcha")

	t.Log("下发的数据中有captcha数据") //记录一些你期望记录的信息
}

func TestImgeCode04(t *testing.T) {
	t.Log("测试使用图片验证码，获得手机号,来下发手机验证码")
	tt.ClearDb()
	c := tt.GetConfig()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, _ := tt.GetRegisterJWT(a.UserName, a.Password)
	phone := tt.RandomPhone()
	e := httpexpect.New(t, c.API)
	e.GET("/api/user/bindphone/imgcode/"+phone).WithHeader("Authorization", jwt).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("data").Value("data").Object().ContainsKey("captcha")

	imgecode := tt.GetFromRedis("captcha:" + phone)
	color.Red("code=%v", imgecode)

	{
		e := httpexpect.New(t, c.API)
		e.GET("/api/user/bindphone/code").WithQuery("phone", phone).WithQuery("code", imgecode).WithHeader("Authorization", jwt).Expect().
			Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0)
	}
}

func TestImgeCode05(t *testing.T) {
	t.Log("测试使用图片验证码，获得手机号,来下发手机验证码,然后绑定手机")
	tt.ClearDb()
	c := tt.GetConfig()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, _ := tt.GetRegisterJWT(a.UserName, a.Password)
	phone := tt.RandomPhone()
	e := httpexpect.New(t, c.API)
	e.GET("/api/user/bindphone/imgcode/"+phone).WithHeader("Authorization", jwt).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("data").Value("data").Object().ContainsKey("captcha")

	imgecode := tt.GetFromRedis("captcha:" + phone)
	color.Red("code=%v", imgecode)

	{
		e := httpexpect.New(t, c.API)
		e.GET("/api/user/bindphone/code").WithQuery("phone", phone).WithQuery("code", imgecode).WithHeader("Authorization", jwt).Expect().
			Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0)
	}

	code := tt.FindLastOneCode(phone)
	{
		e := httpexpect.New(t, c.API)
		e.GET("/api/user/bindphone").WithQuery("phone", phone).WithQuery("code", code).WithHeader("Authorization", jwt).Expect().
			Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0)
	}
}

func TestImgeCode06(t *testing.T) {
	t.Log("测试一个手机号不能绑定多个账号")
	tt.ClearDb()
	c := tt.GetConfig()
	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, _ := tt.GetRegisterJWT(a.UserName, a.Password)
	phone := tt.RandomPhone()
	e := httpexpect.New(t, c.API)
	e.GET("/api/user/bindphone/imgcode/"+phone).WithHeader("Authorization", jwt).Expect().
		Status(http.StatusOK).JSON().Object().ContainsKey("data").Value("data").Object().ContainsKey("captcha")

	imgecode := tt.GetFromRedis("captcha:" + phone)
	color.Red("code=%v", imgecode)

	{
		e := httpexpect.New(t, c.API)
		e.GET("/api/user/bindphone/code").WithQuery("phone", phone).WithQuery("code", imgecode).WithHeader("Authorization", jwt).Expect().
			Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0)
	}

	code := tt.FindLastOneCode(phone)
	{
		e := httpexpect.New(t, c.API)
		e.GET("/api/user/bindphone").WithQuery("phone", phone).WithQuery("code", code).WithHeader("Authorization", jwt).Expect().
			Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0)
	}

	{
		b := SomeStructWithTags{}

		err := faker.FakeData(&b)
		if err != nil {
			panic(err)
		}

		jwt, _ := tt.GetRegisterJWT(b.UserName, b.Password)
		e := httpexpect.New(t, c.API)
		e.GET("/api/user/bindphone/imgcode/"+phone).WithHeader("Authorization", jwt).Expect().
			Status(http.StatusOK).JSON().Object().ContainsKey("data").Value("data").Object().ContainsKey("captcha")

		imgecode := tt.GetFromRedis("captcha:" + phone)
		color.Red("code=%v", imgecode)

		{
			e := httpexpect.New(t, c.API)
			e.GET("/api/user/bindphone/code").WithQuery("phone", phone).WithQuery("code", imgecode).WithHeader("Authorization", jwt).Expect().
				Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueEqual("code", 0)
		}

		code := tt.FindLastOneCode(phone)
		{
			e := httpexpect.New(t, c.API)
			e.GET("/api/user/bindphone").WithQuery("phone", phone).WithQuery("code", code).WithHeader("Authorization", jwt).Expect().
				Status(http.StatusOK).JSON().Object().ContainsKey("code").ValueNotEqual("code", 0)
		}
	}
}
