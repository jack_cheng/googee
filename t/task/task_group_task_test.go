package t

import (
	"fmt"
	tt "googee/t"
	"net/http"
	"testing"
	"time"

	"github.com/bxcodec/faker/v3"
	"github.com/gavv/httpexpect/v2"
	"gopkg.in/go-playground/assert.v1"
)

func TestTask01(t *testing.T) {
	fmt.Println("Testing")
	t.Log("测试领取任务奖励")

	tt.ClearDb()
	fmt.Println("清空数据完毕1")
	tt.InsertConditionConfig("coin", "金币")
	tt.InsertConditionConfig("level", "等级")

	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}
	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, id := tt.GetRegisterJWT(a.UserName, a.Password)
	fmt.Println("jwt=", jwt)

	user_id := id
	var test_group_id int64

	//累计金币消耗
	{
		group_id := tt.InsertTaskGroup("chengzhang", "coin_cost_total", "累计金币消耗", nil)
		fmt.Println("group_id: ", group_id)

		test_group_id = group_id

		//创建一个任务
		task_id1 := tt.InsertTask(group_id, "cost_100", "累计消耗100金币", nil)
		//插入任务的条件
		tt.InsertTaskCondition(group_id, task_id1, "coin", 100)
		//插入奖励
		tt.InsertTaskBonus(task_id1, "coin", "金币", 100)
		tt.InsertTaskBonus(task_id1, "score", "积分", 100)

		//创建一个任务
		task_id2 := tt.InsertTask(group_id, "cost_1000", "累计消耗1000金币", nil)
		//插入任务的条件
		tt.InsertTaskCondition(group_id, task_id2, "coin", 1000)
		//插入奖励
		tt.InsertTaskBonus(task_id2, "coin", "金币", 200)
		tt.InsertTaskBonus(task_id2, "score", "积分", 200)

		//插入任务进度
		tt.InsertTaskProgress(group_id, user_id, "coin", 1000, nil)
	}

	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)

	e.GET(fmt.Sprintf("/api/task/group/%d", test_group_id)).
		WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data")
	// Object().
	// ContainsKey("cateList").Value("cateList").
	// Array().Length().Equal(3)

	e.GET("/api/task/require").
		WithQuery("taskId", 1).
		WithQuery("batchId", 1).
		WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data")

	time.Sleep(5 * time.Second)
	//领取任务1的奖励
	a1 := tt.GetAccountByUserId(user_id)

	assert.Equal(t, a1.Coin, int64(100))
	assert.Equal(t, a1.Score, int64(100))

	//领取任务2的奖励
	e.GET("/api/task/require").
		WithQuery("taskId", 2).
		WithQuery("batchId", 1).
		WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data")
	time.Sleep(5 * time.Second)
	//领取任务1的奖励
	a2 := tt.GetAccountByUserId(user_id)
	assert.Equal(t, a2.Coin, int64(300))
	assert.Equal(t, a2.Score, int64(300))
}

func TestTask02(t *testing.T) {
	fmt.Println("Testing")
	t.Log("测试领取任务奖励,以后当前2个任务都变成已领取状态")

	tt.ClearDb()
	tt.InsertConditionConfig("coin", "金币")
	tt.InsertConditionConfig("level", "等级")

	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}
	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, id := tt.GetRegisterJWT(a.UserName, a.Password)
	fmt.Println("jwt=", jwt)

	user_id := id
	var test_group_id int64

	//累计金币消耗
	{
		group_id := tt.InsertTaskGroup("chengzhang", "coin_cost_total", "累计金币消耗", nil)
		fmt.Println("group_id: ", group_id)

		test_group_id = group_id

		//创建一个任务
		task_id1 := tt.InsertTask(group_id, "cost_100", "累计消耗100金币", nil)
		//插入任务的条件
		tt.InsertTaskCondition(group_id, task_id1, "coin", 100)
		//插入奖励
		tt.InsertTaskBonus(task_id1, "coin", "金币", 100)
		tt.InsertTaskBonus(task_id1, "score", "积分", 100)

		//创建一个任务
		task_id2 := tt.InsertTask(group_id, "cost_1000", "累计消耗1000金币", nil)
		//插入任务的条件
		tt.InsertTaskCondition(group_id, task_id2, "coin", 1000)
		//插入奖励
		tt.InsertTaskBonus(task_id2, "coin", "金币", 200)
		tt.InsertTaskBonus(task_id2, "score", "积分", 200)

		//插入任务进度
		tt.InsertTaskProgress(group_id, user_id, "coin", 1000, nil)
	}

	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)

	//领取之前所有的任务都是可完成可领取状态
	e.GET(fmt.Sprintf("/api/task/group/%d", test_group_id)).
		WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().
		ContainsKey("tasks").Value("tasks").Array().
		Element(0).Object().ContainsKey("status").ValueEqual("status", 0)

	e.GET(fmt.Sprintf("/api/task/group/%d", test_group_id)).
		WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().
		ContainsKey("tasks").Value("tasks").Array().
		Element(1).Object().ContainsKey("status").ValueEqual("status", 0)

	//领取任务1的奖励
	e.GET("/api/task/require").
		WithQuery("taskId", 1).
		WithQuery("batchId", 1).
		WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data")

	//领取任务2的奖励
	e.GET("/api/task/require").
		WithQuery("taskId", 2).
		WithQuery("batchId", 1).
		WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data")

	//领取之后所有的任务都是可完成已经领取状态
	e.GET(fmt.Sprintf("/api/task/group/%d", test_group_id)).
		WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().
		ContainsKey("tasks").Value("tasks").Array().
		Element(0).Object().ContainsKey("status").ValueEqual("status", 1)

	e.GET(fmt.Sprintf("/api/task/group/%d", test_group_id)).
		WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().
		ContainsKey("tasks").Value("tasks").Array().
		Element(1).Object().ContainsKey("status").ValueEqual("status", 1)
}

func TestTask03(t *testing.T) {
	fmt.Println("Testing")
	t.Log("测试领取任务奖励,2个任务领取以后，不能重复领取")

	tt.ClearDb()
	tt.InsertConditionConfig("coin", "金币")
	tt.InsertConditionConfig("level", "等级")

	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}
	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, id := tt.GetRegisterJWT(a.UserName, a.Password)
	fmt.Println("jwt=", jwt)

	user_id := id

	//累计金币消耗
	{
		group_id := tt.InsertTaskGroup("chengzhang", "coin_cost_total", "累计金币消耗", nil)
		fmt.Println("group_id: ", group_id)

		//创建一个任务
		task_id1 := tt.InsertTask(group_id, "cost_100", "累计消耗100金币", nil)
		//插入任务的条件
		tt.InsertTaskCondition(group_id, task_id1, "coin", 100)
		//插入奖励
		tt.InsertTaskBonus(task_id1, "coin", "金币", 100)
		tt.InsertTaskBonus(task_id1, "score", "积分", 100)

		//创建一个任务
		task_id2 := tt.InsertTask(group_id, "cost_1000", "累计消耗1000金币", nil)
		//插入任务的条件
		tt.InsertTaskCondition(group_id, task_id2, "coin", 1000)
		//插入奖励
		tt.InsertTaskBonus(task_id2, "coin", "金币", 200)
		tt.InsertTaskBonus(task_id2, "score", "积分", 200)

		//插入任务进度
		tt.InsertTaskProgress(group_id, user_id, "coin", 1000, nil)
	}

	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)

	//领取任务1的奖励
	e.GET("/api/task/require").
		WithQuery("taskId", 1).
		WithQuery("batchId", 1).
		WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data")

	//领取任务2的奖励
	e.GET("/api/task/require").
		WithQuery("taskId", 2).
		WithQuery("batchId", 1).
		WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data")

	//领取任务1的奖励
	e.GET("/api/task/require").
		WithQuery("taskId", 1).
		WithQuery("batchId", 1).
		WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueNotEqual("code", 0)

	//领取任务2的奖励
	e.GET("/api/task/require").
		WithQuery("taskId", 2).
		WithQuery("batchId", 1).
		WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueNotEqual("code", 0)
}
