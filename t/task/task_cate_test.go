package t

import (
	"fmt"
	tt "googee/t"
	"net/http"
	"testing"

	"github.com/bxcodec/faker/v3"
	"github.com/gavv/httpexpect/v2"
)

func TestTaskCate01(t *testing.T) {
	fmt.Println("Testing")
	t.Log("测试获取指定分类的任务")
	tt.ClearDb()
	fmt.Println("清空数据完毕")
	tt.InsertConditionConfig("coin", "金币")
	tt.InsertConditionConfig("level", "等级")

	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, id := tt.GetRegisterJWT(a.UserName, a.Password)
	fmt.Println("jwt=", jwt)

	user_id := id
	//等级提升
	{
		//创建任务
		group_id := tt.InsertTaskGroup("chengzhang", "level_up", "等级养成", nil)

		//创建一个任务
		task_id1 := tt.InsertTask(group_id, "level1", "等级1级", nil)
		//插入任务的条件
		tt.InsertTaskCondition(group_id, task_id1, "level", 1)
		//插入奖励
		tt.InsertTaskBonus(task_id1, "coin", "金币", 100)

		//创建一个任务
		task_id2 := tt.InsertTask(group_id, "level2", "等级2级", nil)
		//插入任务的条件
		tt.InsertTaskCondition(group_id, task_id2, "level", 2)
		//插入奖励
		tt.InsertTaskBonus(task_id2, "coin", "金币", 200)

		//插入任务进度
		tt.InsertTaskProgress(group_id, user_id, "level", 1, nil)
	}

	//累计金币消耗
	{
		group_id := tt.InsertTaskGroup("chengzhang", "coin_cost_total", "累计金币消耗", nil)
		fmt.Println("group_id: ", group_id)

		//创建一个任务
		task_id1 := tt.InsertTask(group_id, "cost_100", "累计消耗100金币", nil)
		//插入任务的条件
		tt.InsertTaskCondition(group_id, task_id1, "coin", 100)
		//插入奖励
		tt.InsertTaskBonus(task_id1, "yuanbao", "元宝", 1)

		//创建一个任务
		task_id2 := tt.InsertTask(group_id, "cost_1000", "累计消耗1000金币", nil)
		//插入任务的条件
		tt.InsertTaskCondition(group_id, task_id2, "coin", 1000)
		//插入奖励
		tt.InsertTaskBonus(task_id2, "yuanbao", "元宝", 200)

		//插入任务进度
		tt.InsertTaskProgress(group_id, user_id, "coin", 1, nil)
	}

	//每日消耗金币
	{
		group_id := tt.InsertTaskGroup("chengzhang", "coin_cost_daily", "每日金币消耗", func(option *tt.TaskGroupOptions) {
			option.Cron = "0 0 * * *"
			option.Typ = "Y"
			option.Valid = "Y"
			option.ValidDays = 2
		})

		//创建一个任务
		task_id1 := tt.InsertTask(group_id, "cost_100", "单日消耗100金币", nil)
		//插入任务的条件
		tt.InsertTaskCondition(group_id, task_id1, "coin", 100)
		//插入奖励
		tt.InsertTaskBonus(task_id1, "yuanbao", "元宝", 1)

		//创建一个任务
		task_id2 := tt.InsertTask(group_id, "cost_1000", "单日消耗1000金币", nil)
		//插入任务的条件
		tt.InsertTaskCondition(group_id, task_id2, "coin", 1000)
		//插入奖励
		tt.InsertTaskBonus(task_id2, "yuanbao", "元宝", 200)

		//插入任务进度
		tt.InsertTaskProgress(group_id, user_id, "coin", 1, nil)
	}

	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)

	e.GET("/api/task/group/list/chengzhang").
		WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().
		ContainsKey("cateList").Value("cateList").
		Array().Length().Equal(3)
}

func TestTaskCate02(t *testing.T) {
	fmt.Println("Testing")
	t.Log("测试获取指定分类的任务")
	tt.ClearDb()
	fmt.Println("清空数据完毕")
	tt.InsertConditionConfig("coin", "金币")
	tt.InsertConditionConfig("level", "等级")

	type SomeStructWithTags struct {
		Password    string `faker:"password,len=6"`
		UserName    string `faker:"username,len=6" `
		PhoneNumber string `faker:"e_164_phone_number"`
	}

	a := SomeStructWithTags{}
	err := faker.FakeData(&a)
	if err != nil {
		panic(err)
	}

	jwt, id := tt.GetRegisterJWT(a.UserName, a.Password)
	fmt.Println("jwt=", jwt)

	user_id := id
	//等级提升
	{
		//创建任务
		group_id := tt.InsertTaskGroup("chengzhang", "level_up", "等级养成", nil)

		//创建一个任务
		task_id1 := tt.InsertTask(group_id, "level1", "等级1级", nil)
		//插入任务的条件
		tt.InsertTaskCondition(group_id, task_id1, "level", 1)
		//插入奖励
		tt.InsertTaskBonus(task_id1, "coin", "金币", 100)

		//创建一个任务
		task_id2 := tt.InsertTask(group_id, "level2", "等级2级", nil)
		//插入任务的条件
		tt.InsertTaskCondition(group_id, task_id2, "level", 2)
		//插入奖励
		tt.InsertTaskBonus(task_id2, "coin", "金币", 200)

		//插入任务进度
		tt.InsertTaskProgress(group_id, user_id, "level", 1, nil)
	}

	//累计金币消耗
	{
		group_id := tt.InsertTaskGroup("test", "coin_cost_total", "累计金币消耗", nil)
		fmt.Println("group_id: ", group_id)

		//创建一个任务
		task_id1 := tt.InsertTask(group_id, "cost_100", "累计消耗100金币", nil)
		//插入任务的条件
		tt.InsertTaskCondition(group_id, task_id1, "coin", 100)
		//插入奖励
		tt.InsertTaskBonus(task_id1, "yuanbao", "元宝", 1)

		//创建一个任务
		task_id2 := tt.InsertTask(group_id, "cost_1000", "累计消耗1000金币", nil)
		//插入任务的条件
		tt.InsertTaskCondition(group_id, task_id2, "coin", 1000)
		//插入奖励
		tt.InsertTaskBonus(task_id2, "yuanbao", "元宝", 200)

		//插入任务进度
		tt.InsertTaskProgress(group_id, user_id, "coin", 1, nil)
	}

	//每日消耗金币
	{
		group_id := tt.InsertTaskGroup("test", "coin_cost_daily", "每日金币消耗", func(option *tt.TaskGroupOptions) {
			option.Cron = "0 0 * * *"
			option.Typ = "Y"
			option.Valid = "Y"
			option.ValidDays = 2
		})

		//创建一个任务
		task_id1 := tt.InsertTask(group_id, "cost_100", "单日消耗100金币", nil)
		//插入任务的条件
		tt.InsertTaskCondition(group_id, task_id1, "coin", 100)
		//插入奖励
		tt.InsertTaskBonus(task_id1, "yuanbao", "元宝", 1)

		//创建一个任务
		task_id2 := tt.InsertTask(group_id, "cost_1000", "单日消耗1000金币", nil)
		//插入任务的条件
		tt.InsertTaskCondition(group_id, task_id2, "coin", 1000)
		//插入奖励
		tt.InsertTaskBonus(task_id2, "yuanbao", "元宝", 200)

		//插入任务进度
		tt.InsertTaskProgress(group_id, user_id, "coin", 1, nil)
	}

	c := tt.GetConfig()
	e := httpexpect.New(t, c.API)

	e.GET("/api/task/group/list/test").
		WithHeader("Authorization", jwt).Expect().Status(http.StatusOK).
		JSON().
		Object().ContainsKey("code").ValueEqual("code", 0).
		ContainsKey("data").Value("data").Object().
		ContainsKey("cateList").Value("cateList").
		Array().Length().Equal(2)
}
