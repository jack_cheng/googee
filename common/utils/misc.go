package utils

import (
	"errors"
	"fmt"
	"math/rand"
	"strings"
	"time"

	"googee/common/cronexpr"

	"github.com/jinzhu/copier"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func findMinAndMax(a []time.Duration) (min time.Duration, max time.Duration) {
	min = a[0]
	max = a[0]
	for _, value := range a {
		if value < min {
			min = value
		}
		if value > max {
			max = value
		}
	}
	return min, max
}

func PrevCronTime(cronline string, t time.Time) time.Time {
	expr := cronexpr.MustParse(cronline)
	next1 := expr.Next(t)
	prevTime := expr.Next(t)

	m := make(map[time.Duration]int)

	for i := 0; i < 10; i++ {
		nextTime := expr.Next(prevTime)
		duration := nextTime.Sub(prevTime)
		prevTime = nextTime

		if _, ok := m[duration]; !ok {
			m[duration] = i
		}
	}

	keys := make([]time.Duration, 0, len(m))
	for k := range m {
		keys = append(keys, k)
	}

	_, b := findMinAndMax(keys)
	x0 := t.Add(-b)

	prev0 := expr.Next(x0)

	for i := 0; i < 10; i++ {
		if expr.Next(prev0) == next1 {
			return prev0
		}
		prev0 = expr.Next(prev0)
	}

	return time.Time{}
}

func NextCronTime(cronline string, t time.Time) time.Time {
	expr := cronexpr.MustParse(cronline)
	nextTime := expr.Next(t)
	return nextTime
}

func MaxInt64(x, y int64) int64 {
	if x < y {
		return y
	}
	return x
}

func MaxInt(x, y int64) int64 {
	if x < y {
		return y
	}
	return x
}

// Min returns the smaller of x or y.
func MinInt64(x, y int64) int64 {
	if x > y {
		return y
	}
	return y
}

func MinInt(x, y int) int {
	if x > y {
		return y
	}
	return y
}

func AbsInt(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func AbsInt64(x int64) int64 {
	if x < 0 {
		return -x
	}
	return x
}

func RandomNumber(min int, max int) int {
	return rand.Intn(max-min+1) + min
}

// FirstUpper 字符串首字母大写
func FirstUpper(s string) string {
	if s == "" {
		return ""
	}
	return strings.ToUpper(s[:1]) + s[1:]
}

// FirstLower 字符串首字母小写
func FirstLower(s string) string {
	if s == "" {
		return ""
	}
	return strings.ToLower(s[:1]) + s[1:]
}

const (
	Int64 int64 = 0
)

func MyCopier(toValue interface{}, fromValue interface{}) {
	copier.CopyWithOption(toValue, fromValue, copier.Option{
		IgnoreEmpty: true,
		DeepCopy:    false,
		Converters: []copier.TypeConverter{
			{
				SrcType: time.Time{},
				DstType: copier.String,
				Fn: func(src interface{}) (interface{}, error) {
					s, ok := src.(time.Time)
					if !ok {
						return nil, errors.New("src type not matching")
					}
					return s.Format(time.RFC3339), nil
				},
			},
			{
				SrcType: Int64,
				DstType: copier.String,
				Fn: func(src interface{}) (interface{}, error) {
					return fmt.Sprintf("%v", src), nil
				},
			},
		},
	})
}
