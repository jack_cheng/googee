package errorx

import "google.golang.org/grpc/codes"

const OK uint32 = 0

//系统内部未知错误
const INTERNAL_ERROR = 1

//user错误
const USER_ERROR = 100000
const LEADERBOARD_ERROR = 100100
const ACCOUNT_ERROR = 100200
const ADDRBOOK_ERROR = 100300
const INBOX_ERROR = 100400
const TASK_ERROR = 100500

var ERROR_MAP = make(map[int]string)

const (
	UserNotExist codes.Code = 1001
)

func init() {
	ERROR_MAP[3] = "参数错误"

	//用户模块
	ERROR_MAP[USER_ERROR+1] = "账号或者密码错误"
	ERROR_MAP[USER_ERROR+2] = "参数错误"
	ERROR_MAP[USER_ERROR+3] = "账号已经存在"
	ERROR_MAP[USER_ERROR+4] = "手机号格式错误"
	ERROR_MAP[USER_ERROR+5] = "手机号已经绑定"
	ERROR_MAP[USER_ERROR+6] = "账号注销失败"
	ERROR_MAP[USER_ERROR+7] = "账号已经注销无法登录"
	ERROR_MAP[USER_ERROR+8] = "图片验证码错误"
	ERROR_MAP[USER_ERROR+9] = "短信已经发送请不要反复重发"
	ERROR_MAP[USER_ERROR+10] = "手机验证码无效"
	ERROR_MAP[USER_ERROR+11] = "当前账号已经绑定了手机"

	//排行榜模块
	ERROR_MAP[LEADERBOARD_ERROR+1] = "排行无效"

	//账户模块
	ERROR_MAP[ACCOUNT_ERROR+1] = "账户不存在"

	//地址模块
	ERROR_MAP[ADDRBOOK_ERROR+1] = "地址不存在"
	ERROR_MAP[ADDRBOOK_ERROR+2] = "创建收货地址失败"
	ERROR_MAP[ADDRBOOK_ERROR+3] = "收货地址的手机号格式不争取"
	ERROR_MAP[ADDRBOOK_ERROR+4] = "省市区不正确"

	//收件箱
	ERROR_MAP[INBOX_ERROR+1] = "消息不存在"
	ERROR_MAP[INBOX_ERROR+2] = "用户不存在"
	ERROR_MAP[INBOX_ERROR+3] = "参数错误"

	//任务错误信息
	ERROR_MAP[TASK_ERROR+1] = "任务不存在"
	ERROR_MAP[TASK_ERROR+2] = "没有找任务数据"
	ERROR_MAP[TASK_ERROR+3] = "任务没有完成不能领取"
	ERROR_MAP[TASK_ERROR+4] = "任务已经领取"

}
