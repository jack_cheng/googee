package tool

import (
	"regexp"
)

func IsStrAllLetter(str string) bool {
	match, _ := regexp.MatchString(`^[A-Za-z]+$`, str)
	return match
}

func IsStrAllLowerLetter(str string) bool {
	match, _ := regexp.MatchString(`^[a-z]+$`, str)
	return match
}

func IsStrAllCaptialLetter(str string) bool {
	match, _ := regexp.MatchString(`^[A-Z]+$`, str)
	return match
}

func IsStrAllNumberAndLetter(str string) bool {
	match, _ := regexp.MatchString(`^[A-Za-z0-9]+$`, str)
	return match
}

func VerifyMobileFormat(mobileNum string) bool {
	regular := "^((13[0-9])|(14[5,7])|(15[0-3,5-9])|(17[0,3,5-8])|(18[0-9])|166|198|199|(147))\\d{8}$"
	reg := regexp.MustCompile(regular)
	return reg.MatchString(mobileNum)
}

func VerifyEmailFormat(email string) bool {
	//pattern := `\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*` //匹配电子邮箱
	pattern := `^[0-9a-z][_.0-9a-z-]{0,31}@([0-9a-z][0-9a-z-]{0,30}[0-9a-z]\.){1,4}[a-z]{2,4}$`

	reg := regexp.MustCompile(pattern)
	return reg.MatchString(email)
}
