package tool

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestInvCode1(t *testing.T) {
	assert.Equal(t, "d3ihcF", GetInvCodeByUIDUniqueNew(100000000, 6))
	assert.Equal(t, "giuqiI", GetInvCodeByUIDUniqueNew(100000001, 6))
	assert.Equal(t, "jxGzoL", GetInvCodeByUIDUniqueNew(100000002, 6))
}
